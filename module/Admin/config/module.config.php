<?php
namespace Admin;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
	'controllers' => [
		'factories' => [
			Controller\AdminController::class => Factory\ControllerFactory::class,
			Controller\UserController::class => Factory\ControllerFactory::class,
			Controller\CategoryController::class => Factory\ControllerFactory::class,
            Controller\CustomerController::class => Factory\ControllerFactory::class,
            Controller\InstitutionController::class => Factory\ControllerFactory::class,
            Controller\BaselineController::class => Factory\ControllerFactory::class,
            Controller\NeedsController::class => Factory\ControllerFactory::class,
            Controller\CBAController::class => Factory\ControllerFactory::class,
		    Controller\StaffController::class => Factory\ControllerFactory::class
		],
	],
	'service_manager' => [
    	'factories' => [
			Model\UserTable::class => InvokableFactory::class,
			Model\CategoryTable::class => InvokableFactory::class,
            Model\InstitutionTable::class => InvokableFactory::class,
            Model\CustomerTable::class => InvokableFactory::class,
    	    Model\AdminSurveyTable::class => InvokableFactory::class,
    	    Model\StaffTable::class => InvokableFactory::class,
            'navigation' => Zend\Navigation\Service\DefaultNavigationFactory::class,
    	],			
	],
    'router' => [
    	'routes' => [
    		'admin' => [
    			'type' => 'Literal',
    			'options' => [
    				'route'    => '/admin',
    				'defaults' => [
    					'controller' => Controller\AdminController::class,
    					'action'     => 'index',
    				],
    			],
    			'may_terminate' => true,
    			'child_routes' => [
    				'login' => [
    					'type' => 'literal',
    					'options' => [
    						'route'    => '/login',
		    				'defaults' => [
		    					'controller' => Controller\AdminController::class,
		    					'action'     => 'login',
		    				],
    					]
    				],
    				'dashboard' => [
    						'type' => 'literal',
    						'options' => [
    								'route'    => '/dashboard',
    								'defaults' => [
    									'controller' => Controller\AdminController::class,
    									'action'     => 'dashboard',
    								],
    						]
    				],
    				'logout' => [
    						'type' => 'literal',
    						'options' => [
    								'route'    => '/logout',
    								'defaults' => [
    										'controller' => Controller\AdminController::class,
    										'action'     => 'logout',
    								],
    						]
    				],
    				'profile' => [
    						'type' => 'literal',
    						'options' => [
    								'route'    => '/profile',
    								'defaults' => [
    										'controller' => Controller\AdminController::class,
    										'action'     => 'profile',
    								],
    						]
    				],
    				'category' => [
    						'type' => 'Segment',
    						'options' => [
    								'route'    => '/category[/:page]',
    								'constraints' => [
    									'page' => '[0-9]+',
    								],
    								'defaults' => [
    										'controller' => Controller\CategoryController::class,
    										'action'     => 'index',
    								],
    						]
    				],
                    'customers' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/customers[/:page]',
                                    'constraints' => [
                                        'page' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                            'controller' => Controller\CustomerController::class,
                                            'action'     => 'index',
                                    ],
                            ]
                            
                    ],
                    'update-customer' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/update-customer[/:id]',
                                    'constraints' => [
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                            'controller' => Controller\CustomerController::class,
                                            'action'     => 'update',
                                    ],
                            ]
                    ],
                    'delete-customer' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/delete-customer[/:id]',
                            'constraints' => [
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                    'controller' => Controller\CustomerController::class,
                                    'action'     => 'delete',
                            ],
                        ]
                    ],

                    'activate-customer' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/activate-customer[/:id]',
                            'constraints' => [
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\CustomerController::class,
                                'action'     => 'activate',
                            ],
                        ]
                    ],

                    'deactivate-customer' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/deactivate-customer[/:id]',
                                    'constraints' => [
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                            'controller' => Controller\CustomerController::class,
                                            'action'     => 'deactivate',
                                    ],
                            ]
                    ],

                    'institutions' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/institutions[/:page]',
                            'constraints' => [
                                'page' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\InstitutionController::class,
                                'action'     => 'index',
                            ],
                        ]
                    ],
                    'delete-institution' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/delete-institution[/:id]',
                            'constraints' => [
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                    'controller' => Controller\InstitutionController::class,
                                    'action'     => 'delete',
                            ],
                        ]
                    ],

                    'activate-institution' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/activate-institution[/:id]',
                            'constraints' => [
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\InstitutionController::class,
                                'action'     => 'activate',
                            ],
                        ]
                    ],

                    'deactivate-institution' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/deactivate-institution[/:id]',
                                    'constraints' => [
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                            'controller' => Controller\InstitutionController::class,
                                            'action'     => 'deactivate',
                                    ],
                            ]
                    ],

                    'baseline-analysis' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/baseline-analysis[/:page]',
                                    'constraints' => [
                                        'page' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                        'controller' => Controller\BaselineController::class,
                                        'action'     => 'index',
                                    ],
                            ]
                    ],

                    'needs-analysis' => [
                            'type' => 'Segment',
                            'options' => [
                                    'route'    => '/needs-analysis[/:page]',
                                    'constraints' => [
                                        'page' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                            'controller' => Controller\NeedsController::class,
                                            'action'     => 'index',
                                    ],
                            ]
                    ],

                    'cba-staff-cost' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-staff-cost[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'staffcost',
                            ],
                        ]
                    ],

                    'cba-process-cost' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-process-cost[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'processcost',
                            ],
                        ]
                    ],
                    'cba-all-agencies' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-all-agencies[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'allagencies',
                            ],
                        ]
                    ],
                    'cba-lta-cost' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-lta-cost[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'ltacost',
                            ],
                        ]
                    ],
                    'cba-labour-cost' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-labour-cost[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'labourcost',
                            ],
                        ]
                    ],
                    'cba-other-cost' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cba-other-cost[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'othercost',
                            ],
                        ]
                    ],
                    'cost-benefits' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/cost-benefits[/:page]',
                            'constraints' => [
                            'page' => '[0-9]+',
                            ],
                            'defaults' => [
                            'controller' => Controller\CBAController::class,
                                'action'     => 'costbenefit',
                            ],
                        ]
                    ],
    			    'manage-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/manage-grade-cost[/:page]',
    			            'constraints' => [
    			                'page' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'managestaffcost',
    			            ],
    			        ]
    			    ],
    			    'add-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/add-grade-cost[/:id]',
    			            'constraints' => [
    			                'id' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'add',
    			            ],
    			        ]
    			    ],
    			    'update-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/update-grade-cost[/:id]',
    			            'constraints' => [
    			                'id' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'update',
    			            ],
    			        ]
    			    ],
    			    'delete-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/delete-grade-cost[/:id]',
    			            'constraints' => [
    			                'id' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'delete',
    			            ],
    			        ]
    			    ],
    			    
    			    'activate-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/activate-grade-cost[/:id]',
    			            'constraints' => [
    			                'id' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'activate',
    			            ],
    			        ]
    			    ],
    			    
    			    'deactivate-grade-cost' => [
    			        'type' => 'Segment',
    			        'options' => [
    			            'route'    => '/deactivate-grade-cost[/:id]',
    			            'constraints' => [
    			                'id' => '[0-9]+',
    			            ],
    			            'defaults' => [
    			                'controller' => Controller\StaffController::class,
    			                'action'     => 'deactivate',
    			            ],
    			        ]
    			    ],
    			]
    		]
    	],
    ],

    'navigation' => [
        'default' => [
            [
                'label' => 'DASHBOARD',
                'route' => 'admin/dashboard',
            ],
            [
                'label' => 'USERS',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'Consultants',
                        'route' => 'admin/institutions',
                    ]
                ]
            ],
            [
                'label' => 'SURVEYS',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'Surveys',
                        'route' => 'admin/institutions',
                    ]
                ],
            ],
            [
                'label' => 'OPERATIONS ANALYSIS ',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'Baseline/Stock Analysis',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'Needs analysis',
                        'route' => 'admin/institutions',
                    ]
                ],
            ],
            [
                'label' => 'STOCK TAKING',
                'route' => 'admin/institutions',
                'class' => 'no-link',
            ],
            [
                'label' => 'COST BENEFIT ANALYSIS',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'CBA - Staff Cost',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA - Process Cost',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA - LTA Cost',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA - Labour Cost',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA - Other Cost',
                        'route' => 'admin/institutions',
                    ]
                ],
            ],
            [
                'label' => 'CBA AND PRIORITIZATION',
                'route' => 'admin/institutions',
                'class' => 'no-link',
            ],
            [
                'label' => 'REPORTING',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'Stock Taking',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA Labour Analysis',
                        'route' => 'admin/institutions',
                    ],
                    [
                        'label' => 'CBA and Prioritization',
                        'route' => 'admin/institutions',
                    ]
                ],
            ],
            [
                'label' => 'CMS',
                'route' => 'admin/institutions',
                'class' => 'no-link',
                'pages' => [
                    [
                        'label' => 'Pages',
                        'route' => 'admin/institutions',
                    ]
                ],
            ],
        ],
    ],

    // The following registers our custom view helper classes.
    'view_helpers' => [
        'factories' => [      
            View\Helper\Breadcrumbs::class => InvokableFactory::class,          
        ],
        'aliases' => [
            'pageBreadcrumbs' => View\Helper\Breadcrumbs::class,
        ]
    ],
    'view_manager' => [
    	'template_map' => [
    		'admin/layout'          => __DIR__ . '/../view/layout/admin.phtml',
    		'admin/admin/index' 	=> __DIR__ . '/../view/admin/admin/index.phtml',
    		'error/404'             => __DIR__ . '/../view/error/404.phtml',
    		'error/index'           => __DIR__ . '/../view/error/index.phtml',
    	],
    	'template_path_stack' => [
    			__DIR__ . '/../view',
    	]
    ],
];