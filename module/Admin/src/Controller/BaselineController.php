<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Model\AdminSurveyTable;
use Calculator\Controller\IndexController as Calculator;

class BaselineController extends AbstractActionController
{
    
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

    /**
     * [__construct description]
     * @param Adapter               $dbAdapter   [description]
     * @param AuthenticationService $authService [description]
     */
	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
        
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
			
        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/admin/login');
        }
        
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::onDispatch()
	 */
	public function onDispatch(\Zend\Mvc\MvcEvent $e)
	{
	    if (!$this->authService->hasIdentity()) {
	        return $this->redirect()->tourl('/admin/login');
	    }
	    
	    return parent::onDispatch($e);
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
        $surveyData = $this->getModel()->getBaselineSurvey();
        if(!empty($surveyData)){
            $iTrator = 0;            
            foreach ($surveyData as $survey){
                
                $unitTxnCost = $this->getModel()->getProcessUnitTxnCostWithoutLTA($survey['survey_id'], $survey['id']);
                $unitTXNCost = !empty($unitTxnCost) ? $unitTxnCost['hourly_cost'] : 0;
                $totalTxnCost = $unitTXNCost * $survey['baseline_quantity_purchased'];
                $totalCost = $totalTxnCost + $survey['baseline_cost'];
                
                $unitTxnCostWithLTA = $this->getModel()->getProcessUnitTxnCostWithLTA($survey['survey_id'], $survey['id']);
                $unitSaving = !empty($unitTxnCostWithLTA) ? ($unitTXNCost - $unitTxnCostWithLTA['hourly_cost']) : 0;
                $totalSaving = $unitSaving * $survey['baseline_quantity_purchased'];
                $annualSavingOnSCE =  (($survey['baseline_cost'] * $survey['target_cost_save']) / 100);
                
                $surveyData[$iTrator]['unit_txn_cost'] = $unitTXNCost;
                $surveyData[$iTrator]['total_txn_cost'] = $totalTxnCost;
                $surveyData[$iTrator]['total_cost'] = $totalCost;
                $surveyData[$iTrator]['unit_saving'] = $unitSaving;
                $surveyData[$iTrator]['total_saving'] = $totalSaving;
                $surveyData[$iTrator]['annual_saving_on_sce'] = $annualSavingOnSCE;
                
                $iTrator++;
                
            }
        }
        
        $view = new ViewModel();
        $view->setVariable('baseline_data', $surveyData);
        return $view;
    }

    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel( $table = 'users_survey_stocktaking' ){
        $tableGateway = new TableGateway($table, $this->dbAdapter);
        return new AdminSurveyTable($tableGateway);
    }

}
