<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Form\ProfileForm;
use Admin\Form\Filter\ProfileFilter;
use Admin\Model\UserTable;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\TableGateway\TableGateway;

class UserController extends AbstractActionController
{	
	
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
    	
    }
    
    /**
     * Profile Action
     */
    public function profileAction(){    
    	$userDetail = $this->getModel()->getUser(1);
    	
    	$request = $this->getRequest();
    	 
    	
    	$profileForm = new ProfileForm('profileForm');
    	$profileForm->setInputFilter( new ProfileFilter() );
    	$profileForm->setData ( [ 
			'email' => $userDetail->email,
			'first_name' => $userDetail->first_name,
			'last_name' => $userDetail->last_name 
		] );
    	 
    	if($request->isPost()){
    	
    		$data = $request->getPost();
    		$profileForm->setData($data);
    		 
    		if($profileForm->isValid()){
    			$data = $profileForm->getData();
    			$data['id'] = 1;
    			unset($data['submit']);
    			$this->getModel()->saveUser($data);
    		}else{
    			$errors = $profileForm->getMessages();
    		}
    	}
    	
    	$view = new ViewModel();
    	$view->setVariable('profileForm', $profileForm);
    	$view->setVariable('userDetail', $userDetail);
    	return $view;
    }
    
    private function getModel(){
    	$tableGateway = new TableGateway('users', $this->dbAdapter);
    	return new UserTable($tableGateway);
    }
}
