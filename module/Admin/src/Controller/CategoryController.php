<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Admin\Model\CategoryTable;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\TableGateway\TableGateway;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter as PaginationAdapter;
use Zend\Paginator\Paginator;

class CategoryController extends AbstractActionController{	

	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

	public function __construct(Adapter $dbAdapter, AuthenticationService $authService){
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction(){
    	
    	//$categories = $this->getModel()->fetchAll();
    	
    	// Grab the paginator from the AlbumTable:
    	$categories = $this->getModel()->fetchAll(TRUE);
    	$paginator = new Paginator(new PaginationAdapter\ArrayAdapter($categories));
    	
    	// Set the current page to what has been passed in query string,
    	// or to 1 if none is set, or the page is invalid:
    	$page = $this->params()->fromRoute('page', 1);
    	$page = ($page < 1) ? 1 : $page;
    	$paginator->setCurrentPageNumber($page);
    	
    	// Set the number of items per page to 10:
    	$paginator->setItemCountPerPage(10);
    	
    	$view = new ViewModel();
    	$view->setVariable('categories', $paginator);
    	return $view;	
    	
    }
    
    private function getModel(){
    	$tableGateway = new TableGateway('categories', $this->dbAdapter);
    	return new CategoryTable($tableGateway);
    }
    
}
