<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Form\LoginForm;
use Admin\Form\Filter\LoginFilter;
use Zend\Validator\File\Md5;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Form\InstutionForm;
use Admin\Form\Filter\InstutionFilter;
use Admin\Model\InstitutionTable;
use Zend\Paginator\Adapter as PaginationAdapter;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\Session\Container;

class CBAController extends AbstractActionController
{
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

    /**
     * [__construct description]
     * @param Adapter               $dbAdapter   [description]
     * @param AuthenticationService $authService [description]
     */
	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
			
        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/admin/login');
        }
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {

        $view = new ViewModel();
        return $view;   

    }
    
    public function costbenefitAction(){
        $view = new ViewModel();
        return $view; 
    }

    public function staffcostAction()
    {

        $view = new ViewModel();
        return $view;   

    }

    public function processcostAction()
    {

        $view = new ViewModel();
        return $view;   

    }

    public function allagenciesAction()
    {

        $view = new ViewModel();
        return $view;   

    }

    public function ltacostAction()
    {

        $view = new ViewModel();
        return $view;   

    }

    public function labourcostAction()
    {

        $view = new ViewModel();
        return $view;   

    }

    public function othercostAction()
    {

        $view = new ViewModel();
        return $view;   

    }
    
    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel(){
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new InstitutionTable($tableGateway);
    }

}
