<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Form\LoginForm;
use Admin\Form\Filter\LoginFilter;
use Zend\Validator\File\Md5;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Form\InstutionForm;
use Admin\Form\Filter\InstutionFilter;
use Admin\Model\InstitutionTable;
use Zend\Paginator\Adapter as PaginationAdapter;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\Session\Container;

class InstitutionController extends AbstractActionController
{
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

    /**
     * [__construct description]
     * @param Adapter               $dbAdapter   [description]
     * @param AuthenticationService $authService [description]
     */
	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
			
        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/admin/login');
        }
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {

        $page = $this->params()->fromRoute('page', 1);
        $page = ($page < 1) ? 1 : $page;

        $session = new Container('institutionSearchFilter');

        $request = $this->getRequest();
        $keyword = '';
        if($request->isPost()){
            $data = $request->getPost();
            if( !empty($data) && isset($data['search']) && isset($data['keyword']) ){
                $session->offsetSet('keyword', $data['keyword']);
                $this->redirect()->tourl('/admin/institutions');
            } else if(isset($data['reset'])){
                $session->offsetUnset('keyword');
                $keyword = '';
            }
        }

        if($session->offsetExists('keyword')){
            $keyword = $session->offsetGet('keyword');
        } 
        
        $arrayAdapter = new \Zend\Paginator\Adapter\ArrayAdapter( $this->getModel()->fetchAll($keyword) );
        $paginator = new \Zend\Paginator\Paginator($arrayAdapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel();
        $view->setVariable('institutions', $paginator);
        $view->setVariable('keyword', $keyword);
        return $view;   

    }
    
    /**
     * [updateAction description]
     * @return [type] [description]
     */
    public function updateAction(){

        $instutionID = $this->params()->fromRoute('id', 0);

        if($instutionID <= 0){ 
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
            $this->redirect()->tourl('/admin/institutions');
        }

        $request = $this->getRequest();         
        $instutionForm = new InstitutionFrom('instutionForm');
        $instutionForm->setInputFilter( new InstutionFilter() );
        $instutionForm->setData ( [ 
            'email' => $userDetail->email,
            'first_name' => $userDetail->first_name,
            'last_name' => $userDetail->last_name 
        ] );
         
        if($request->isPost()){
        
            $data = $request->getPost();
            $instutionForm->setData($data);
             
            if($instutionForm->isValid()){
                $data = $instutionForm->getData();
                $data['id'] = 1;
                unset($data['submit']);
                $this->getModel()->saveUser($data);
            }else{
                $errors = $instutionForm->getMessages();
            }
        }
        
        $view = new ViewModel();
        $view->setVariable('instutionForm', $instutionForm);
        $view->setVariable('userDetail', $userDetail);
        return $view;

    }

    /**
     * [deleteAction description]
     * @return [type] [description]
     */
    public function deleteAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->deleteInstitution($id)){
                $this->flashMessenger()->addMessage('Institution deleted successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/institutions');

    }

    /**
     * [activateAction description]
     * @return [type] [description]
     */
    public function activateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->updateInstitution(array('is_active'=>'1'), array('id'=>(int)$id ))){
                $this->flashMessenger()->addMessage('Institution activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/institutions');

    }

    /**
     * [deactivateAction description]
     * @return [type] [description]
     */
    public function deactivateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->updateInstitution(array('is_active'=>'0'), array('id' => $id))){
                $this->flashMessenger()->addMessage('Institution de-activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/institutions');

    }

    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel(){
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new InstitutionTable($tableGateway);
    }

}
