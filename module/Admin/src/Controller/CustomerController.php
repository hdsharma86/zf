<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Validator\File\Md5;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Model\CustomerTable;
use Zend\Session\Container;
use Admin\Form\CustomerForm as CustomerFrom;
use Admin\Form\Filter\CustomerFilter;

class CustomerController extends AbstractActionController
{
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

    /**
     * [__construct description]
     * @param Adapter               $dbAdapter   [description]
     * @param AuthenticationService $authService [description]
     */
	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;	
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
    	if (!$this->authService->hasIdentity()) {
    		return $this->redirect()->tourl('/admin/login');
    	}

        $page = $this->params()->fromRoute('page', 1);
        $page = ($page < 1) ? 1 : $page;

        $session = new Container('customerSearchFilter');

        $request = $this->getRequest();
        $keyword = '';
        if($request->isPost()){
            $data = $request->getPost();
            if( !empty($data) && isset($data['search']) && isset($data['keyword']) ){
                $session->offsetSet('keyword', $data['keyword']);
                $this->redirect()->tourl('/admin/customers');
            } else if(isset($data['reset'])){
                $session->offsetUnset('keyword');
                $keyword = '';
            }
        }

        if($session->offsetExists('keyword')){
            $keyword = $session->offsetGet('keyword');
        } 
        
        $customers = $this->getModel()->fetchAll($keyword);
        $totalCustomers = count($customers);
        $itemsPerPage = 10;
        if($totalCustomers > 0){
        $rowStat = 'Showing '.(($totalCustomers < $itemsPerPage) ? '1' : ($itemsPerPage * $page)).' to '.(($totalCustomers < $itemsPerPage) ? $totalCustomers : ($itemsPerPage * $page)+10).' of '.$totalCustomers.' entries';
        } else {
            $rowStat = '0 Record(s) Found';
        }

        $arrayAdapter = new \Zend\Paginator\Adapter\ArrayAdapter($customers);
        $paginator = new \Zend\Paginator\Paginator($arrayAdapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($itemsPerPage);

        $view = new ViewModel();
        $view->setVariable('customers', $paginator);
        $view->setVariable('keyword', $keyword);
        $view->setVariable('rowStat', $rowStat);
        return $view; 

    }

    /**
     * [updateAction description]
     * @return [type] [description]
     */
    public function updateAction(){

        $customerID = $this->params()->fromRoute('id', 0);

        if($customerID <= 0){ 
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
            $this->redirect()->tourl('/admin/customers');
        }

        $customerDetail = array();
        $customerDetail = $this->getModel()->getCustomer($customerID);
        $customerForm = new CustomerFrom('customerForm');
        $customerForm->setInputFilter( new CustomerFilter() );

        $request = $this->getRequest(); 
        if($request->isPost()){
        
            $data = $request->getPost();
            $customerForm->setData($data);
            if($customerForm->isValid()){

                $data = $customerForm->getData();
                unset($data['submit']);
               
                $userData = array(
                    'last_modified' => date('Y-m-d H:i:s')
                );

                $profileData = array(
                    'first_name'    => $data['first_name'],
                    'middle_name'   => $data['middle_name'],
                    'last_name'     => $data['last_name'],
                    'contact_type'  => $data['phone_type'],
                    'intl_prefix'   => $data['int_prefix'],
                    'contact_number'=> $data['contact_number'],
                    'address_type'  => $data['address_type'],
                    'address'       => $data['address'],
                    'state'         => $data['state'],
                    'country'       => $data['country'],
                    'city'          => $data['city'],
                    'zip'           => $data['postal_code'],
                    'last_modified' => date('Y-m-d H:i:s')
                );

                if($this->getModel()->updateCustomer($data['customer_id'], $userData, $profileData)){
                    $this->flashMessenger()->addMessage('Customer updated successfully.');
                    $this->redirect()->tourl('/admin/customers');
                } 
                
            }

        }
        
        $countries = $this->getModel()->getCountries();
        $states = $this->getModel()->getStatesByCountry($customerDetail->country);

        $countryList = array();
        if(!empty($countries)){
            $countryList[''] = '-Select Country-';
            foreach ($countries as $country) {
                $countryList[$country['id']] = $country['name'];
            }
        }

        $stateList = array();
        if(!empty($states)){
            $stateList[''] = '-Select State-';
            foreach ($states as $state) {
                $stateList[$state['id']] = $state['name'];
            }
        }

        $customerForm->get('country')->setAttribute('options',$countryList);
        $customerForm->get('state')->setAttribute('options',$stateList);
        $customerForm->setData ([ 
            'customer_id'   => $customerID,
            'first_name'    => $customerDetail->first_name,
            'middle_name'   => $customerDetail->middle_name ,
            'last_name'     => $customerDetail->last_name ,
            'email'         => $customerDetail->email,
            'address_type'  => $customerDetail->address_type,
            'address'       => $customerDetail->address,
            'country'       => $customerDetail->country,
            'state'         => $customerDetail->state,
            'city'          => $customerDetail->city,
            'phone_type'    => $customerDetail->contact_type,
            'int_prefix'    => $customerDetail->intl_prefix,
            'contact_number'=> $customerDetail->contact_number,
            'postal_code'   => $customerDetail->zip
        ]);

        $view = new ViewModel();
        $view->setVariable('customerForm', $customerForm);
        return $view;

    }

    /**
     * [deleteAction description]
     * @return [type] [description]
     */
    public function deleteAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->deleteCustomer($id)){
                $this->flashMessenger()->addMessage('Customer deleted successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/customers');

    }

    /**
     * [activateAction description]
     * @return [type] [description]
     */
    public function activateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->update($id, array('is_active'=>'1'))){
                $this->flashMessenger()->addMessage('Customer activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/customers');

    }

    /**
     * [deactivateAction description]
     * @return [type] [description]
     */
    public function deactivateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel()->update($id, array('is_active'=>'0'))){
                $this->flashMessenger()->addMessage('Customer de-activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/customers');

    }
    
    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel(){
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new CustomerTable($tableGateway);
    }

}
