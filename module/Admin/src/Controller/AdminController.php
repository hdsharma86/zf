<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Form\LoginForm;
use Admin\Form\Filter\LoginFilter;
use Zend\Validator\File\Md5;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Form\ProfileForm;
use Admin\Form\Filter\ProfileFilter;
use Admin\Model\UserTable;

class AdminController extends AbstractActionController
{
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;

	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;
			
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
    	if ($this->authService->hasIdentity()) {
    		$identity = $this->authService->getIdentity();
    		return $this->redirect()->tourl('/admin/dashboard');
    	} else {
        	return $this->redirect()->tourl('/admin/login');
    	}
        
    }
    
    /**
     * Function for admin login...
     */
    public function loginAction(){
    	
        $this->layout('layout/admin_login');

    	if ($this->authService->hasIdentity()) {
    		return $this->redirect()->tourl('/admin/dashboard');
    	}
    	
    	$request = $this->getRequest();
    	
    	$view = new ViewModel();
    	$loginForm = new LoginForm('loginForm');
    	$loginForm->setInputFilter( new LoginFilter() );
    	
    	if($request->isPost()){
    		
    		$data = $request->getPost();
    		$loginForm->setData($data);
    	
    		if($loginForm->isValid()){

    			$data = $loginForm->getData();
    			
    			$email = $data['email'];
    			$userPassword = MD5($data['password']);
    			
    			$this->authAdapter = new AuthAdapter($this->dbAdapter);
    			$this->authAdapter->setTableName('users')->setIdentityColumn('email')->setCredentialColumn('password');
    			$this->authAdapter->setIdentity($email);
    			$this->authAdapter->setCredential($userPassword);
                $this->authAdapter->getDbSelect()->where('type_id = 1');

    			$result = $this->authService->authenticate($this->authAdapter);
    			
    			if ($result->isValid()) {    				
    				$this->flashMessenger()->addMessage(array('success' => 'Login Success.'));
    			} else {
    				$this->flashMessenger()->addMessage(array('error' => 'invalid credentials.'));
    			}
    			
    			return $this->redirect()->tourl('/admin');
    			
    		}else{
    			$errors = $loginForm->getMessages();
    		}
    	}
    	
    	$view->setVariable('loginForm', $loginForm);
    	return $view;
    }

    /**
     * Profile Action
     */
    public function profileAction(){    

        $userDetail = $this->getModel()->getUser(1);
        $request = $this->getRequest();
            
        $profileForm = new ProfileForm('profileForm');
        $profileForm->setInputFilter( new ProfileFilter() );
        $profileForm->setData ( [ 
            'email' => $userDetail['email'],
            'first_name' => $userDetail['first_name'],
            'last_name' => $userDetail['last_name'] 
        ] );
         
        if($request->isPost()){
        
            $data = $request->getPost();
            $profileForm->setData($data);
             
            if($profileForm->isValid()){
                $data = $profileForm->getData();
                $data['id'] = 1;
                unset($data['submit']);
                $this->getModel()->saveUser($data);
            }else{
                $errors = $profileForm->getMessages();
            }
        }
        
        $view = new ViewModel();
        $view->setVariable('profileForm', $profileForm);
        $view->setVariable('userDetail', $userDetail);
        return $view;

    }
    
    /**
     * Function to display admin dashboard after login... 
     */
    public function dashboardAction(){
    	if (!$this->authService->hasIdentity()) {
    		return $this->redirect()->tourl('/admin/login');
    	}
    	return [];
    }
        
    public function logoutAction(){
    	$this->authService->clearIdentity();
    	return $this->redirect()->toUrl('/admin/login');
    }

    private function getModel(){
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new UserTable($tableGateway);
    }
}
