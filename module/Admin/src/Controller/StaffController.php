<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Validator\File\Md5;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Admin\Model\StaffTable;
use Zend\Session\Container;
use Admin\Form\StaffCostForm as StaffCostForm;
use Admin\Form\Filter\StaffCostFilter;

class StaffController extends AbstractActionController
{
	protected $dbAdapter;
	protected $authAdapter;
	protected $authService;
	private $tblStaffGradesCosts = 'staff_grades_costs';

    /**
     * [__construct description]
     * @param Adapter               $dbAdapter   [description]
     * @param AuthenticationService $authService [description]
     */
	public function __construct(Adapter $dbAdapter, AuthenticationService $authService)
	{
		$this->authService = $authService;
		$this->dbAdapter = $dbAdapter;	
	}
	
	/**
	 * Action call for initilization here...
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
    	if (!$this->authService->hasIdentity()) {
    		return $this->redirect()->tourl('/admin/login');
    	}

        $view = new ViewModel();
        return $view; 

    }

    public function managestaffcostAction(){
        
        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/admin/login');
        }
        
        $page = $this->params()->fromRoute('page', 1);
        $page = ($page < 1) ? 1 : $page;
        
        $session = new Container('customerSearchFilter');
        
        $request = $this->getRequest();
        $keyword = '';
        if($request->isPost()){
            $data = $request->getPost();
            if( !empty($data) && isset($data['search']) && isset($data['keyword']) ){
                $session->offsetSet('keyword', $data['keyword']);
                $this->redirect()->tourl('/admin/manage-grade-cost');
            } else if(isset($data['reset'])){
                $session->offsetUnset('keyword');
                $keyword = '';
            }
        }
        
        if($session->offsetExists('keyword')){
            $keyword = $session->offsetGet('keyword');
        }
        
        $customers = $this->getModel('staff_grades_costs')->fetchAll($keyword);
        $totalGradeCosts = count($customers);
        $itemsPerPage = 10;
        if($totalGradeCosts > 0){
            $rowStat = 'Showing '.(($totalGradeCosts < $itemsPerPage) ? '1' : ($itemsPerPage * $page)).' to '.(($totalGradeCosts < $itemsPerPage) ? $totalGradeCosts : ($itemsPerPage * $page)+10).' of '.$totalGradeCosts.' entries';
        } else {
            $rowStat = '0 Record(s) Found';
        }
        
        $arrayAdapter = new \Zend\Paginator\Adapter\ArrayAdapter($customers);
        $paginator = new \Zend\Paginator\Paginator($arrayAdapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($itemsPerPage);
        
        $view = new ViewModel();
        $view->setVariable('gradeCosts', $paginator);
        $view->setVariable('keyword', $keyword);
        $view->setVariable('rowStat', $rowStat);
        return $view; 
        
    }
    
    /**
     * [updateAction description]
     * @return [type] [description]
     */
    public function updateAction(){

        $costID = $this->params()->fromRoute('id', 0);

        if($costID <= 0){ 
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
            $this->redirect()->tourl('/admin/manage-grade-cost');
        }

        $costDetail = array();
        $costDetail = $this->getModel($this->tblStaffGradesCosts)->getGradeCost($costID);
        $costForm = new StaffCostForm('staffCostForm');
        $costForm->setInputFilter( new StaffCostFilter() );

        $request = $this->getRequest(); 
        if($request->isPost()){
        
            $data = $request->getPost();
            $costForm->setData($data);
            if($costForm->isValid()){

                $data = $costForm->getData();
                unset($data['submit']);
                
                $costData = [
                    'cost' => $data['grade_cost']
                ];
                
                if($this->getModel($this->tblStaffGradesCosts)->updateGradeCost($data['grade_cost_id'], $costData)){
                    $this->flashMessenger()->addMessage('Grade Cost updated successfully.');
                    $this->redirect()->tourl('/admin/manage-grade-cost');
                } 
                
            }

        }
        
        $countries = $this->getModel()->getCountries();
        $grades = $this->getModel()->getGrades();
        
        $countryList = array();
        if(!empty($countries)){
            $countryList[''] = '-Select Country-';
            foreach ($countries as $country) {
                $countryList[$country['id']] = $country['name'];
            }
        }
        
        $gradesList = array();
        if(!empty($grades)){
            $gradesList[''] = '-Select Grade Level-';
            foreach ($grades as $grade) {
                $gradesList[$grade['id']] = $grade['name'];
            }
        }
        
        $costForm->get('country_id')->setAttribute('options',$countryList);
        $costForm->get('grade_id')->setAttribute('options',$gradesList);
        $costForm->get('country_id')->setAttribute('disabled', 'disabled');
        $costForm->get('grade_id')->setAttribute('disabled', 'disabled');
        
        $costForm->setData ([
            'grade_cost_id' => $costDetail['id'],
            'country_id' => $costDetail['country_id'],
            'grade_id' => $costDetail['grade_id'],
            'grade_cost' => $costDetail['cost']
        ]);

        $view = new ViewModel();
        $view->setVariable('costForm', $costForm);
        return $view;

    }
    
    /**
     * [updateAction description]
     * @return [type] [description]
     */
    public function addAction(){
        
        /*$costDetail = array();
        $costDetail = $this->getModel($this->tblStaffGradesCosts)->getGradeCost($costID);*/
        $costForm = new StaffCostForm('staffCostForm');
        $costForm->setInputFilter( new StaffCostFilter() );
        
        $request = $this->getRequest();
        if($request->isPost()){
            
            $data = $request->getPost();
            $costForm->setData($data);
            if($costForm->isValid()){
                
                $data = $costForm->getData();
                unset($data['submit']);
                $costData = [
                    'grade_id'      => $data['grade_id'],
                    'country_id'    => $data['country_id'],
                    'cost'          => $data['grade_cost'],
                    'status'        => 1,
                    'last_modified' => date('Y-m-d H:i:s'),
                    'created_on'    => date('Y-m-d H:i:s')
                ];
                
                $isGradeCostExists = $this->getModel($this->tblStaffGradesCosts)->checkGradeCost($data['grade_id'], $data['country_id']);
                if(!empty($isGradeCostExists)){
                    $this->flashMessenger()->addMessage('Grade Cost exists already, please update the existing one.');
                    $this->redirect()->tourl('/admin/manage-grade-cost');
                } else if($this->getModel($this->tblStaffGradesCosts)->saveGradeCost($costData)) {
                    $this->flashMessenger()->addMessage('Grade Cost added successfully.');
                    $this->redirect()->tourl('/admin/manage-grade-cost');
                }
                
            }
            
        }
        
        $countries = $this->getModel()->getCountries();
        $grades = $this->getModel()->getGrades();
        
        $countryList = array();
        if(!empty($countries)){
            $countryList[''] = '-Select Country-';
            foreach ($countries as $country) {
                $countryList[$country['id']] = $country['name'];
            }
        }
        
        $gradesList = array();
        if(!empty($grades)){
            $gradesList[''] = '-Select Grade Level-';
            foreach ($grades as $grade) {
                $gradesList[$grade['id']] = $grade['name'];
            }
        }
        
        $costForm->get('country_id')->setAttribute('options',$countryList);
        $costForm->get('grade_id')->setAttribute('options',$gradesList);
                
        $view = new ViewModel();
        $view->setVariable('costForm', $costForm);
        return $view;
        
    }

    /**
     * [deleteAction description]
     * @return [type] [description]
     */
    public function deleteAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel($this->tblStaffGradesCosts)->deleteGradeCost($id)){
                $this->flashMessenger()->addMessage('Grade Cost deleted successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/manage-grade-cost');

    }

    /**
     * [activateAction description]
     * @return [type] [description]
     */
    public function activateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel($this->tblStaffGradesCosts)->update($id, array('status'=>'1'))){
                $this->flashMessenger()->addMessage('Grade Cost activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/manage-grade-cost');

    }

    /**
     * [deactivateAction description]
     * @return [type] [description]
     */
    public function deactivateAction(){

        $id = $this->params()->fromRoute('id', 0);

        if($id > 0){
            if($this->getModel($this->tblStaffGradesCosts)->update($id, array('status'=>'0'))){
                $this->flashMessenger()->addMessage('Grade Cost de-activated successfully.');
            } else {
               $this->flashMessenger()->addMessage('Something went wrong, please try again.');
            }
        } else {
            $this->flashMessenger()->addMessage('Invalid identity supplied.');
        }
        $this->redirect()->tourl('/admin/manage-grade-cost');

    }
    
    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel( $table = "staff" ){
        $tableGateway = new TableGateway($table, $this->dbAdapter);
        return new StaffTable($tableGateway);
    }

}
