<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;

class ControllerFactory implements FactoryInterface
{
	/**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return ListController
     */
    public function __invoke(ContainerInterface $serviceManager, $controllerName, array $options = null)
    {
        if(!class_exists($controllerName)) {
        	throw new ServiceNotFoundException("Requested controller name '".$controllerName."' does not exist.");
        }

        $dbAdapter = $serviceManager->get(\Zend\Db\Adapter\AdapterInterface::class);
        
        //$repository = $serviceManager->get(UserTable::class);
        
        $authService = new AuthenticationService();
        
        return new $controllerName($dbAdapter, $authService);
    }
    
}