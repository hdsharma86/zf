<?php
namespace Admin\Model;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\dbAdapter;
use Zend\Db\Adapter\AdapterInterface as AdapterInterface;

class CustomerTable
{
    private $tableGateway;
    private $dbAdapter;
    
    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }
    
    /**
     * Fetch all Customers...
     * @return [type] [description]
     */
    public function fetchAll( $keyword = '' )
    {   

        $searchWhere = '';
        if(!empty($keyword)){
            $searchWhere = "(users_profile.first_name like '%".$keyword."%' OR users_profile.last_name like '%".$keyword."%' OR users.email like '%".$keyword."%') AND "; 
        }
          
        $sqlQuery = "SELECT users.*, users_profile.first_name, users_profile.middle_name, users_profile.last_name, users_profile.contact_number, users_profile.address, users_profile.state, users_profile.city, users_profile.zip, users_profile.country, country.name as country_name, states.name as state_name 
        FROM users 
        LEFT JOIN users_profile ON users_profile.user_id = users.id 
        LEFT JOIN country ON users_profile.country = country.id 
        LEFT JOIN states ON users_profile.state = states.id 
        WHERE ".$searchWhere."users.type_id = '3' AND users.is_deleted = '0'";

        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        
        $resultSet = $resultSet->toArray();

        return $resultSet; 

    }
    
    /**
     * [getCustomer description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getCustomer( $cID = NULL )
    {
    
        $sqlQuery = "SELECT users.*, users_profile.*, users_profile.id as profile_id 
        FROM users 
        LEFT JOIN users_profile ON users_profile.user_id = users.id 
        WHERE users.type_id = '3' AND users.is_deleted = '0' AND users.id = {$cID}";

        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $row = $resultSet->current();
        
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $cID
            ));
        }

        return $row;

    }
    
    /**
     * [saveCustomer description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function saveCustomer($data = [])
    {
        if(!empty($data)){
            $id = (int) $data['id'];
            if ($id === 0) {
                $this->tableGateway->insert($data);
                return;
            }
            if (!$this->getUser($id)) {
                throw new RuntimeException(sprintf(
                    'Cannot update User with identifier %d; does not exist',
                    $id
                ));
            }
            $this->tableGateway->update($data, ['id' => $id]);
        }
    }

    /**
     * [deleteCustomer description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteCustomer( $id = NULL )
    {
        if (empty($id) || $id<=0 || is_bool($id)) {
            throw new \InvalidArgumentException("Invalid Argument: Not allowd string|int|bool here");
        }
        //return $this->tableGateway->delete(['id' => (int) $id]);
        $this->tableGateway->update(['is_deleted'=>1], ['id' => $id]);
    }

    /**
     * [update description]
     * @param  [type] $set   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function updateCustomer($customerID = NULL, $data = [], $profileData = [])
    {
        
        if ( $customerID > 0 && !empty($data) && !empty($profileData) ) {

            $this->tableGateway->update($data, ['id' => $customerID]);

            $adapter = $this->tableGateway->getAdapter(); 
            $userProfileTable = new TableGateway('users_profile', $adapter); 
            $userProfileTable->update($profileData, ['user_id' => $customerID]);
            return true;

        } else {
            throw new \InvalidArgumentException("Invalid supply of data detected...");
        }

    }
	
	/**
     * [update description]
     * @param  [type] $set   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function update($customerID = NULL, $data = [])
    {
        
        if ( $customerID > 0 && !empty($data) ) {

            $this->tableGateway->update($data, ['id' => $customerID]);
            return true;

        } else {
            throw new \InvalidArgumentException("Invalid supply of data detected...");
        }

    }

    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getCountries(){
        $sqlQuery = "SELECT * FROM country";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }

    /**
     * [getStates description]
     * @return [type] [description]
     */
    public function getStates(){
        $sqlQuery = "SELECT * FROM states";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();        
        return $resultSet; 
    }

    public function getStatesByCountry( $countryID = NULL ){
        if($countryID > 0){
            $sqlQuery = "SELECT * FROM states WHERE country_id = ".(int)$countryID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();        
            return $resultSet; 
        }
    }
}