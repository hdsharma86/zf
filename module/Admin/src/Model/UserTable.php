<?php
namespace Admin\Model;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class UserTable
{
    private $tableGateway;
    private $dbAdapter;
    
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }
    
    public function fetchAll($paginated = false)
    {
        if ($paginated) {
            return $this->fetchPaginatedResults();
        }
        return $this->tableGateway->select();
    }
    
    private function fetchPaginatedResults()
    {
        // Create a new Select object for the table:
        $select = new Select($this->tableGateway->getTable());
        // Create a new result set based on the User entity:
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new User());
        // Create a new pagination adapter object:
        $paginatorAdapter = new DbSelect(
        // our configured select object:
            $select,
            // the adapter to run it against:
            $this->tableGateway->getAdapter(),
            // the result set to hydrate:
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }
    
    public function getUser($id)
    {

        $id     = (int) $id;
        if(!empty($id)){
            $select = new \Zend\Db\Sql\Select;
            $select->from('users');
            $select->columns(array('*'));
            $select->join('users_profile', "users.id = users_profile.user_id", array('*'), 'left');
            //echo $select->getSqlString();
            $resultSet = $this->tableGateway->selectWith($select);
            $resultSet = $resultSet->toArray();
            return $resultSet[0]; 
        } else {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d', $id
            ));
        }
        
        /*$rowset = $this->tableGateway->select(['id' => $id]);
        $row    = $rowset->current();
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }
        return $row;*/
    }

    public function getUserData(){

        $sqlQuery = "SELECT * FROM users";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        print_r($resultSet); die;
    }
    
    public function saveUser($data = [])
    {
    	if(!empty($data)){
	        $id = (int) $data['id'];
	        if ($id === 0) {
	            $this->tableGateway->insert($data);
	            return;
	        }
	        if (!$this->getUser($id)) {
	            throw new RuntimeException(sprintf(
	                'Cannot update User with identifier %d; does not exist',
	                $id
	            ));
	        }
	        $this->tableGateway->update($data, ['id' => $id]);
    	}
    }
    
    public function deleteUser($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}