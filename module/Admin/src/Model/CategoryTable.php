<?php
namespace Admin\Model;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter;
use Zend\Paginator\Paginator;

class CategoryTable
{
    private $tableGateway;
    
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll()
    {
       return $this->tableGateway->select()->toArray();       
    }
    
    public function getCategory($id = NULL)
    {
        $id     = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row    = $rowset->current();
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }
        return (array) $row;
    }
    
    public function saveCategory($data = [])
    {
    	if(!empty($data)){
	        $id = (int) $data['id'];
	        if ($id === 0) {
	            $this->tableGateway->insert($data);
	            return;
	        }
	        if (!$this->getUser($id)) {
	            throw new RuntimeException(sprintf(
	                'Cannot update User with identifier %d; does not exist',
	                $id
	            ));
	        }
	        $this->tableGateway->update($data, ['id' => $id]);
    	}
    }
    
    public function deleteCategory($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}