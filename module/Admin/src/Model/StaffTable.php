<?php
namespace Admin\Model;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\dbAdapter;
use Zend\Db\Adapter\AdapterInterface as AdapterInterface;

class StaffTable
{
    private $tableGateway;
    private $dbAdapter;
    
    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }
    
    /**
     * Fetch all GradeCosts...
     * @return [type] [description]
     */
    public function fetchAll( $keyword = '' )
    {   

        $searchWhere = '';
        if(!empty($keyword)){
            $searchWhere = "(country.name like '%".$keyword."%' OR staff_grades.name like '%".$keyword."%') AND "; 
        }
          
        $sqlQuery = "SELECT staff_grades_costs.*, country.name as country_name, staff_grades.name as grade_level  
FROM `staff_grades_costs` 
LEFT JOIN country ON country.id = staff_grades_costs.country_id
LEFT JOIN staff_grades ON staff_grades.id = staff_grades_costs.grade_id
WHERE ".$searchWhere."1 ORDER BY staff_grades_costs.last_modified DESC";

        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        
        $resultSet = $resultSet->toArray();

        return $resultSet; 

    }
    
    /**
     * [getGradeCost description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getGradeCost( $cID = NULL )
    {
    
        $sqlQuery = "SELECT * FROM `staff_grades_costs` WHERE id = {$cID}";

        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $row = $resultSet->current();
        
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $cID
            ));
        }

        return $row;

    }
    
    /**
     * [saveGradeCost description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function saveGradeCost($data = [])
    {
        if(!empty($data)){
            $this->tableGateway->insert($data);
            return true;
        }
    }

    /**
     * [deleteGradeCost description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteGradeCost( $id = NULL )
    {
        if ($id<=0) {
            throw new \InvalidArgumentException("Invalid Argument: Not allowd string|int|bool here");
        }
        return $this->tableGateway->delete(['id' => (int) $id]);
    }

    /**
     * [update description]
     * @param  [type] $set   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function updateGradeCost($costID = NULL, $data = [])
    {
        
        if ( $costID > 0 && !empty($data) ) {

            $this->tableGateway->update($data, ['id' => $costID]);
            return true;

        } else {
            throw new \InvalidArgumentException("Invalid supply of data detected...");
        }

    }
	
	/**
     * [update description]
     * @param  [type] $set   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function update($customerID = NULL, $data = [])
    {
        
        if ( $customerID > 0 && !empty($data) ) {

            $this->tableGateway->update($data, ['id' => $customerID]);
            return true;

        } else {
            throw new \InvalidArgumentException("Invalid supply of data detected...");
        }

    }
    
    /**
     * 
     * @param unknown $gradeID
     * @param unknown $countryID
     * @return unknown
     */
    public function checkGradeCost( $gradeID = NULL, $countryID = NULL )
    {
        if(!empty($gradeID) && !empty($countryID)){
            $sqlQuery = "SELECT * FROM `staff_grades_costs` WHERE grade_id = {$gradeID} AND country_id = {$countryID}";
            
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            
            $resultSet = $resultSet->toArray();
            
            return isset($resultSet[0]) ? $resultSet[0] : $resultSet;
        }
        
    }

    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getCountries(){
        $sqlQuery = "SELECT * FROM country";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }
    
    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getGrades(){
        $sqlQuery = "SELECT * FROM staff_grades";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }
        
    /**
     * [getStates description]
     * @return [type] [description]
     */
    public function getStates(){
        $sqlQuery = "SELECT * FROM states";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();        
        return $resultSet; 
    }

    public function getStatesByCountry( $countryID = NULL ){
        if($countryID > 0){
            $sqlQuery = "SELECT * FROM states WHERE country_id = ".(int)$countryID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();        
            return $resultSet; 
        }
    }
}