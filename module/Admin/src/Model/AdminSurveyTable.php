<?php
namespace Admin\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

class AdminSurveyTable
{
    private $tableGateway;
    private $dbAdapter;
    
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }
    
    public function fetchAll()
    {        
        $sqlQuery = "SELECT * FROM users_survey_stocktaking";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();        
    }
    
    /**
     * @param unknown $customerID
     */
    public function getBaselineSurvey(){
        $sqlQuery = "SELECT users_survey.user_id, users_survey.country_id, users_survey.agency_id, users_survey_stocktaking.*, country.name as country_name, services.title as service_area, agencies.title as agency_name, services_childs.title as existing_service, CONCAT(users_profile.first_name,' ',users_profile.middle_name,' ',users_profile.last_name) as customer_name FROM users_survey
RIGHT JOIN users_survey_stocktaking ON users_survey.id = users_survey_stocktaking.survey_id
LEFT JOIN country ON country.id = users_survey.country_id
LEFT JOIN services ON services.id = users_survey_stocktaking.service_id
LEFT JOIN agencies ON agencies.id = users_survey.agency_id
LEFT JOIN services_childs ON services_childs.id = users_survey_stocktaking.service_child_id
LEFT JOIN users_profile ON users_profile.user_id = users_survey.user_id";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }
    
   /**
    * 
    * @param unknown $surveyID
    * @param unknown $stockID
    * @return unknown
    */
    public function getProcessUnitTxnCostWithoutLTA($surveyID = NULL, $stockID = NULL){
        if(!empty($surveyID) && !empty($stockID)){
            $sqlQuery = "SELECT FORMAT(SUM((((staff_grades_costs.cost/12)/21.75)/7.5) * users_survey_processcost.time_required),2
) as hourly_cost
FROM `users_survey_processcost` 
LEFT JOIN staff_grades_costs ON staff_grades_costs.grade_id = users_survey_processcost.grade_level
where users_survey_processcost.stock_id = {$stockID} and users_survey_processcost.survey_id = {$surveyID}  
GROUP BY users_survey_processcost.stock_id";
			//if($stockID == 2){echo $sqlQuery; die;}
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return isset($resultSet[0])?$resultSet[0]:$resultSet;
        }
    }
    
    /**
     * 
     * @param unknown $surveyID
     * @param unknown $stockID
     * @return unknown
     */
    public function getProcessUnitTxnCostWithLTA($surveyID = NULL, $stockID = NULL){
        if(!empty($surveyID) && !empty($stockID)){
            $sqlQuery = "SELECT FORMAT(SUM((((staff_grades_costs.cost/12)/21.75)/7.5) * users_survey_processcost.time_required),2
) as hourly_cost
FROM `users_survey_processcost`
LEFT JOIN staff_grades_costs ON staff_grades_costs.grade_id = users_survey_processcost.grade_level
where users_survey_processcost.stock_id = {$stockID} and users_survey_processcost.survey_id = {$surveyID} AND users_survey_processcost.has_LTA = '1'
GROUP BY users_survey_processcost.stock_id";
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return isset($resultSet[0])?$resultSet[0]:$resultSet;
        }
    }
    
}