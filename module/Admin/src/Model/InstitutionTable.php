<?php
namespace Admin\Model;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter;
use Zend\Paginator\Paginator;

class InstitutionTable
{
    private $tableGateway;
    private $dbAdapter;
    
    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }
    
    /**
     * Fetch all Institutions...
     * @return [type] [description]
     */
    public function fetchAll( $keyword = '' )
    {
        //return $this->tableGateway->select()->toArray();    
        $searchWhere = '';
        if(!empty($keyword)){
            $searchWhere = "(users_profile.first_name like '%".$keyword."%' OR users_profile.last_name like '%".$keyword."%' OR users.email like '%".$keyword."%') AND "; 
        }
          
        $sqlQuery = "SELECT users.*, users_profile.first_name, users_profile.last_name, users_profile.contact_number, users_profile.address, users_profile.state, users_profile.city FROM users LEFT JOIN users_profile ON users_profile.user_id = users.id WHERE ".$searchWhere."users.type_id = '2' AND users.is_deleted = '0'";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        //return $resultSet = $resultSet->toArray();

        /*$resultSet = $this->tableGateway->select(function (Select $select){
            
            $select->columns(array('id','type_id','email','username','password','token','is_active','is_deleted','last_modified','created_on'));
            $select->join('users_profile', 'users_profile.user_id = users.id', array('first_name', 'last_name', 'contact_number', 'address', 'state', 'city'), 'left');
            $select->where("users.type_id = '2'");
            $select->order(array('id asc')); 
            
        });*/
        
        //$resultSet->buffer();
        //$resultSet->next();
        //  $sql = $this->tableGateway->getSql(); 
        //echo "<pre>"; print_r($sql);
        //die;

        $resultSet = $resultSet->toArray();
        
        return $resultSet; 

    }
    
    /**
     * [getInstitution description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getInstitution( $id = NULL )
    {

        $resultSet = $this->tableGateway->select(function(Select $select) {
            $select->join('users_profile', 'users_profile.user_id = users.id', array('first_name'));
            $select->where('users.id', $id);
            //$select->order('users_profile ASC')->limit(2);
        });
        $row = $resultSet->current();

        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return (array) $row;
    }
    
    /**
     * [saveInstitution description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function saveInstitution($data = [])
    {
        if(!empty($data)){
            $id = (int) $data['id'];
            if ($id === 0) {
                $this->tableGateway->insert($data);
                return;
            }
            if (!$this->getUser($id)) {
                throw new RuntimeException(sprintf(
                    'Cannot update User with identifier %d; does not exist',
                    $id
                ));
            }
            $this->tableGateway->update($data, ['id' => $id]);
        }
    }

    /**
     * [deleteInstitution description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function deleteInstitution( $id = NULL )
    {
        if (empty($id) || $id<=0 || is_bool($id)) {
            throw new \InvalidArgumentException("Invalid Argument: Not allowd string|int|bool here");
        }
        return $this->tableGateway->delete(['id' => (int) $id]);
    }

    /**
     * [update description]
     * @param  [type] $set   [description]
     * @param  [type] $where [description]
     * @return [type]        [description]
     */
    public function updateInstitution($set, $where = null)
    {
        if (is_numeric($where) || is_bool($where)) {
            throw new \InvalidArgumentException("Not allowd string|int|bool here");
        }
        return $this->tableGateway->update($set, $where);
    }

}