<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin\Form\Filter;

use Zend\InputFilter\InputFilter;

class StaffCostFilter extends InputFilter {
	public function __construct() {

		$isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
		$invalidEmail = \Zend\Validator\EmailAddress::INVALID_FORMAT;
				
		$this->add ( [
		    'name' => 'country_id',
		    'required' => true,
		    'validators' => [
		        [
		            'name' => 'NotEmpty',
		            'options' => [
		                'messages' => [
		                    $isEmpty => 'Please select Country.'
		                ]
		            ],
		            'break_chain_on_failure' => true
		        ]
		    ]
		] );
		
		$this->add ( [
		    'name' => 'grade_id',
		    'required' => true,
		    'validators' => [
		        [
		            'name' => 'NotEmpty',
		            'options' => [
		                'messages' => [
		                    $isEmpty => 'Please select Staff Grade Level.'
		                ]
		            ],
		            'break_chain_on_failure' => true
		        ]
		    ]
		] );
		
		$this->add ( [
		    'name' => 'grade_cost',
		    'required' => true,
		    'filters' => [
		        [
		            'name' => 'StripTags'
		        ],
		        [
		            'name' => 'StringTrim'
		        ]
		    ],
		    'validators' => [
		        [
		            'name' => 'NotEmpty',
		            'options' => [
		                'messages' => [
		                    $isEmpty => 'Grade Cost can\'t be empty.'
		                ]
		            ],
		            'break_chain_on_failure' => true
		        ]
		    ]
		] );
	}
}