<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Admin\Form\Filter;

use Zend\InputFilter\InputFilter;

class InstitutionFilter extends InputFilter {
	public function __construct() {
		$isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
		$invalidEmail = \Zend\Validator\EmailAddress::INVALID_FORMAT;
		
		$this->add ( [ 
				'name' => 'email',
				'required' => true,
				'filters' => [ 
						[ 
								'name' => 'StripTags' 
						],
						[ 
								'name' => 'StringTrim' 
						] 
				],
				'validators' => [ 
						[ 
								'name' => 'NotEmpty',
								'options' => [ 
										'messages' => [ 
												$isEmpty => 'Email can not be empty.' 
										] 
								],
								'break_chain_on_failure' => true 
						],
						[ 
								'name' => 'EmailAddress',
								'options' => [ 
										'messages' => [ 
												$invalidEmail => 'Enter Valid Email Address.' 
										] 
								] 
						] 
				] 
		] );
		$this->add ( [ 
				'name' => 'first_name',
				'required' => true,
				'filters' => [ 
						[ 
								'name' => 'StripTags' 
						],
						[ 
								'name' => 'StringTrim' 
						] 
				],
				'validators' => [ 
						[ 
								'name' => 'NotEmpty',
								'options' => [ 
										'messages' => [ 
												$isEmpty => 'First Name can not be empty.' 
										] 
								],
								'break_chain_on_failure' => true 
						]
				] 
		] );
		
		$this->add ( [
				'name' => 'last_name',
				'required' => true,
				'filters' => [
						[
								'name' => 'StripTags'
						],
						[
								'name' => 'StringTrim'
						]
				],
				'validators' => [
						[
								'name' => 'NotEmpty',
								'options' => [
									'messages' => [
										$isEmpty => 'Last Name can not be empty.'
									]
								],
								'break_chain_on_failure' => true
						]
				]
		] );
	}
}