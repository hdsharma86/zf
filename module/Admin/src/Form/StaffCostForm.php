<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class StaffCostForm extends Form {
 
    public function __construct($name) {
        
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        
        $this->add(array(
            'name' => 'grade_cost_id',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'grade_cost_id'
            ),
        ));

        $this->add(array(
            'name' => 'country_id',
            'type' => 'Select',
            'options' => array(
                'label' => ' Country ',
                'value_options' => array(
                    '' => '-Select Country-'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'country_id',
                'class' => 'form-control'
            ),
        ));
        
        $this->add(array(
            'name' => 'grade_id',
            'type' => 'Select',
            'options' => array(
                'label' => ' Staff Grade ',
                'value_options' => array(
                    '' => '-Select Staff Grade-'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'grade_id',
                'class' => 'form-control'
            ),
        ));
        
        $this->add(array(
    		'name' => 'grade_cost',
    		'type' => 'text',
    		'options' => array(
    			'label' => 'Grade Cost',
    			'placeholder' => 'Grade Cost'
    		),
    		'attributes' => array(
    			'class' => 'form-control',
                'id' => 'grade_cost'
    		),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
            	'class' => 'btn btn-primary'
            ),
        ));
    }
}