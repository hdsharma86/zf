<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
//use Zend\Form\Element\Csrf;
 
class InstutionForm extends Form {
 
    public function __construct($name) {
        
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
 
        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'options' => array(
                'label' => 'Email',
                'id' => 'email',
                'placeholder' => 'example@example.com'	
            ),
        	'attributes' => array(
        		'class' => 'form-control'
        	),
        ));
        
        $this->add(array(
        	'name' => 'first_name',
        	'type' => 'text',
        	'options' => array(
        		'label' => 'First Name',
        		'id' => 'email',
        		'placeholder' => 'First Name'
        	),
        	'attributes' => array(
        		'class' => 'form-control'
        	),
        ));
        
        $this->add(array(
        		'name' => 'last_name',
        		'type' => 'text',
        		'options' => array(
        			'label' => 'Last Name',
        			'id' => 'email',
        			'placeholder' => 'Last Name'
        		),
        		'attributes' => array(
        			'class' => 'form-control'
        		),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
            	'class' => 'btn btn-primary'
            ),
        ));
    }
}