<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
//use Zend\Form\Element\Csrf;
 
class LoginForm extends Form {
 
    public function __construct($name) {
        
        parent::__construct($name);
        $this->setAttribute('method', 'post');
 
        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'options' => array(
                'label' => 'Email',
                'id' => 'email',
                'placeholder' => 'example@example.com'	
            ),
        	'attributes' => array(
        		'class' => 'form-control'
        	),
        ));
 
       $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'options' => array(
                'label' => 'Password',
                'id' => 'password',
                'placeholder' => '**********'
            ),
       		'attributes' => array(
       				'class' => 'form-control'
       		),
       ));
       
       /*$this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'loginCsrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 3600
                )
            )
        ));*/
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
            	'class' => 'btn btn btn-greensea'
            ),
        ));
    }
}