<?= $this->doctype() ?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <?= $this->headTitle('Dona')->setSeparator(' - ')->setAutoEscape(false) ?>

        <?= $this->headMeta()
            ->appendName('viewport', 'width=device-width, initial-scale=1.0')
            ->appendHttpEquiv('X-UA-Compatible', 'IE=edge')
        ?>

        <!-- Le styles -->
        <?= $this->headLink(['rel' => 'shortcut icon', 'type' => 'image/vnd.microsoft.icon', 'href' => $this->basePath() . '/img/favicon.ico'])
            ->prependStylesheet($this->basePath('css/bootstrap.min.css'))
            ->prependStylesheet($this->basePath('font-awesome/4.5.0/css/font-awesome.min.css'))
            ->prependStylesheet($this->basePath('css/fonts.googleapis.com.css'))
            ->prependStylesheet($this->basePath('css/ace.min.css'))
            ->prependStylesheet($this->basePath('css/ace-rtl.min.css'))
            ->prependStylesheet($this->basePath('css/ace.min.css'))
        ?>
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" />
		<![endif]-->
        <!-- Scripts -->
        
        <!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		
        <?= $this->headScript()
            ->prependFile($this->basePath('js/bootstrap.min.js'))
            ->prependFile($this->basePath('js/jquery-2.1.4.min.js'))
        ?> 

    </head>
    <body>
    	<div class="header">
		     <div class="container">
		        <div class="row">
		           <div class="col-md-5">
		              <!-- Logo -->
		              <div class="logo">
		                 <h1><a href="<?php echo $this->url('admin'); ?>">Dona</a></h1>
		              </div>
		           </div>
		           <div class="col-md-2 pull-right">
		           	  <?php if($this->identity()): ?>
		              <div class="navbar navbar-inverse" role="banner">
		                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
		                    <ul class="nav navbar-nav">
		                      <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
		                        <ul class="dropdown-menu animated fadeInUp">
		                          <li><a href="<?php echo $this->url('admin/profile'); ?>">Profile</a></li>
		                          <li><a href="<?php echo $this->url('admin/logout'); ?>">Logout</a></li>
		                        </ul>
		                      </li>
		                    </ul>
		                  </nav>
		              </div>
		              <?php endif; ?>
		           </div>
		        </div>
		     </div>
		</div>
        <div class="page-content">
            <?= $this->content ?>
        </div>
        <hr>
        <footer>
            <p>&copy; 2005 - <?= date('Y') ?> by Yapapp India Pvt. Ltd. All rights reserved.</p>
        </footer> 
        <?= $this->inlineScript() ?>
    </body>
</html>
