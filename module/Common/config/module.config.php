<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Common;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'common' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/common',
                    'defaults' => [
                        'controller' => Controller\CommonController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'ajax-requests' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/ajax-requests',
                    'defaults' => [
                        'controller' => Controller\CommonAjaxController::class,
                        'action'     => 'index',
                    ],
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\CommonController::class => Factory\CommonControllerFactory::class,
            Controller\CommonAjaxController::class => Factory\CommonControllerFactory::class
        ],
    ],
    'service_manager' => [
        'factories' => [
            Model\CommonAjaxTable::class => InvokableFactory::class,
            Model\CommonTable::class => InvokableFactory::class
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'common/layout'         => __DIR__ . '/../view/layout/common.phtml',
            'common/common/index'   => __DIR__ . '/../view/common/common/index.phtml',
            'error/404'             => __DIR__ . '/../view/error/404.phtml',
            'error/index'           => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
                __DIR__ . '/../view',
        ]
    ],
];
