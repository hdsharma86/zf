<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Common\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Model\CustomerTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Common\Model\CommonAjaxTable;


class CommonAjaxController extends AbstractActionController
{
	protected $dbAdapter;
    protected $authAdapter;
    protected $authService;

    public function __construct(Adapter $dbAdapter, AuthenticationService $authService) {
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function indexAction()
    {
        
    	$response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
        	$requestData = $request->getPost();
        	if(!empty($requestData)){
        		switch ($requestData['action']) {
				    case 'states_by_country':
				        $response = $this->getStatesByCountry($requestData);
				        break;
				    case 'get_countries':
				        $response = $this->getCountries($requestData);
				        break;
				    case 'get_agencies':
				        $response = $this->getAgencies($requestData);
				        break;
				    case 'get_services':
				        $response = $this->getServices($requestData);
				        break;
				    case 'get_existing_services':
				        $response = $this->getChildServices($requestData);
				        break;	
				    case 'get_existing_customer_survey':
				        $response = $this->getCustomerSurveyByServices($requestData);
				        break;
				    case 'get_grades':
				        $response = $this->getGrades($requestData);
				        break;	
				    case 'get_process_steps':
				        $response = $this->getProcessSteps($requestData);
				        break;	
				    case 'save_process_steps':
				        $response = $this->saveProcessSteps($requestData);
				        break;
				    case 'remove_cost':
				        $response = $this->removeCost($requestData);
				        break;
				    default;
				}
        	}
        }
       
        $jsonModel = new JsonModel();
        $jsonModel->setTerminal(true);
        $data = array('status' => 'FAILED','data' => '');
        if(!empty($response)){
            $data = array('status' => 'SUCCESS','data' => $response);
        }
        return new $jsonModel($data);
    }

    /**
     * [getStatesByCountry description]
     * @return [type] [description]
     */
    private function getStatesByCountry($requestData){

    	$return = [];
        $countryID = $requestData['countryId'];
    	$states = $this->getModel()->getStatesByCountry($countryID);
        $stateArr = [];
    	if(!empty($states)){
    		foreach($states as $state){
    			$stateArr[$state['id']] = $state['name'];
    		}
    	}
        $return['states'] = $stateArr;
        
        $countryPhoneCode = $this->getModel()->getCountryPhoneCode($countryID);
        if(!empty($countryPhoneCode)){
            $return['country_phone_code'] = $countryPhoneCode['phonecode'];
        }

    	return $return;

    }
    
    /**
     * 
     * @param unknown $requestData
     * @return unknown[]|unknown[][]
     */
    private function getCountries($requestData){
        
        $return = [];
        $countries = $this->getModel()->getCountries();
        if(!empty($countries)){
            $return['0'] = "-Select Country-";
            foreach($countries as $country){
                $return[$country['id']] = $country['name'];
            }
        }
        return $return;
        
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array|unknown
     */
    private function getAgencies($requestData){
        
        $return = [];
        $agencies = $this->getModel()->getAgencies();
        if(!empty($agencies)){
            $return['0'] = "-Select Agency-";
            foreach($agencies as $agency){
                $return[$agency['id']] = $agency['title'];
            }
        }
        return $return;
        
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array|unknown
     */
    private function getServices($requestData){
        
        $return = [];
        $services = $this->getModel()->getServices();
        if(!empty($services)){
            $return['0'] = "-Select Service-";
            foreach($services as $service){
                $return[$service['id']] = $service['title'];
            }
        }
        return $return;
        
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array|unknown
     */
    private function getChildServices($requestData){
        
        $return = [];
        $services = $this->getModel()->getChildServices($requestData['parent_id']);
        if(!empty($services)){
            $return['0'] = "-Select Existing Service-";
            foreach($services as $service){
                $return[$service['id']] = $service['title'];
            }
        }
        return $return;
        
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array
     */
    private function getCustomerSurveyByServices($requestData){
        if(!empty($requestData)){
            $return = [];
            $return = $this->getModel()->getBaselineSurveyByServices($requestData['service_area'], $requestData['child_service_area'], $requestData['customer_id']);
            return $return;
        }
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array
     */
    private function getGrades($requestData){
        if(!empty($requestData)){
            $return = [];
            $return = $this->getModel()->getGrades();
            return $return;
        }
    }
    
    /**
     * 
     * @param unknown $requestData
     * @return array
     */
    private function getProcessSteps($requestData){
        if(!empty($requestData)){
            $return = [];
            $return = $this->getModel()->getProcessSteps($requestData['stock_id']);
            return $return;
        }
    }
    
    /**
     * @param unknown $requestData
     * @return array
     */
    private function saveProcessSteps($requestData){
        if(!empty($requestData)){
            $return = [];
            $data = [
                'user_id'       => $requestData['user_id'],
                'survey_id'     => $requestData['survey_id'],
                'stock_id'      => $requestData['stock_id'],
                'has_LTA'       => isset($requestData['has_LTA']) ? $requestData['has_LTA']:0,
                'process_step'  => $requestData['process_step'],
                'staff_involved'=> $requestData['staff_involved'],
                'grade_level'   => $requestData['grade_level'],
                'time_required' => $requestData['time_required'],
                'last_modified' => date('Y-m-d H:i:s'),
                'created_on'    => date('Y-m-d H:i:s')
            ];
            
            //$isGradeCostExists = $this->getModel('users_survey_processcost')->checkGradeCost($requestData['survey_id'],$requestData['stock_id'],$requestData['grade_level']);
            $isGradeCostExists = [];
            if(empty($isGradeCostExists)){
                $return = $this->getModel('users_survey_processcost')->saveProcessSteps($data);
            } else {
                $return = ['message'=>'Grade cost already exists, please add another.'];
            }
            return $return;
        }
    }
    
    /**
     * @param unknown $requestData
     */
    private function removeCost($requestData){
        if(!empty($requestData)){
            $return = [];
            $return = $this->getModel('users_survey_processcost')->removeCost($requestData['cost_id']);
            return $return;
        }
    }
    
    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel($table='users') {
        $tableGateway = new TableGateway($table, $this->dbAdapter);
        return new CommonAjaxTable($tableGateway);
    }

}
