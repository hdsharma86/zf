<?php

namespace Common\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;

class CommonController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    /**
     * [getPassword description]
     * @param  integer $length [description]
     * @return [type]          [description]
     */
    public static function getPassword( $length = 8 ) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * [sendMail description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public static function sendMail( $data = array() ){

        if(!empty($data)){
            $message = new Message();
            $message->addTo($data['TO'])
                    ->addFrom($data['FROM'])
                    ->setSubject($data['SUBJECT']);
                    //->setBody($data['CONTENT']);
            
            $html = new MimePart($data['CONTENT']);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->addPart($html);

            $message->setBody($body);


            $transport = new SendmailTransport();
            return $transport->send($message);
        }
        
    }
    
    /**
     * Function to convert an array to a list...
     * @param array $dataArray
     * @param string $index
     * @param string $value
     * @return []
     */
    public static function arrayToList($dataArray = [], $index = '', $value = '', $default = ''){
        $return = [];
        if(!empty($dataArray) && $index != '' && $value != ''){
            if(!empty($default)){
                $return[''] = $default;
            }
            foreach ($dataArray as $data) {
                $return[$data[$index]] = $data[$value];
            }
        }
        return $return;
    }

}
