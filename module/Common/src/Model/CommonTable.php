<?php

namespace Common\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter;
use Zend\Paginator\Paginator;

class CommonTable {

    private $tableGateway;

    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }

    /**
     * [save description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function save($data = []) {
        if (!empty($data)) {
            $this->tableGateway->insert($data);
            return;
        }
    }

    /**
     * [saveCustomers description]
     * @param  array  $data        [description]
     * @param  array  $profileData [description]
     * @return [type]              [description]
     */
    public function saveCustomers($data = array(), $profileData = array()) {

        $customerID = NULL;
         if (!empty($data)) {
            $this->tableGateway->insert($data);
            $customerID = $this->tableGateway->lastInsertValue;
        }

        if($customerID > 0 && !empty($profileData)){
            $adapter = $this->tableGateway->getAdapter(); 
            $profileData['user_id'] = $customerID;
            $userProfileTable = new TableGateway('users_profile', $adapter); 
            $userProfileTable->insert($profileData);
        }
        return $customerID;

    }

     /**
     * Function to check customer in database
     * @param type $email
     * @return array
     */
    public function checkCustomers($email) {
        $sqlQuery = "SELECT id,email FROM users WHERE email = '" . $email . "' AND is_deleted = 0";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }
    
    /**
     * Function to reset password
     * @param type $email
     * @param type $password
     * @return type
     */
    public function changePassword($email, $password) {
        return $this->tableGateway->update(array('password' => $password), array('email' =>  $email));
    }

    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getCountries(){
        $sqlQuery = "SELECT * FROM country";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }

    /**
     * [getStates description]
     * @return [type] [description]
     */
    public function getStates(){
        $sqlQuery = "SELECT * FROM states";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();        
        return $resultSet; 
    }

    /**
     * [getStatesByCountry description]
     * @param  [type] $countryID [description]
     * @return [type]            [description]
     */
    public function getStatesByCountry( $countryID = NULL ){
        if($countryID > 0){
            $sqlQuery = "SELECT * FROM states WHERE country_id = ".(int)$countryID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();        
            return $resultSet; 
        }
    }

    /**
     * [getCountryPhoneCode description]
     * @param  [type] $countryID [description]
     * @return [type]            [description]
     */
    public function getCountryPhoneCode( $countryID = NULL ){
        if($countryID > 0){
            $sqlQuery = "SELECT phonecode FROM country WHERE id = ".(int) $countryID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(1));
            $resultSet = $resultSet->toArray();
            return $resultSet[0]; 
        }
    }

}
