<?php

namespace Common\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter;
use Zend\Paginator\Paginator;

class CommonAjaxTable {

    private $tableGateway;

    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }

    public function saveSurveyForm($data = []) {
        if (!empty($data)) {
            //$userId = (int) $data['id'];
            $this->tableGateway->insert($data);
            return true;
//            if (!$this->getUser($userId)) {
//                throw new RuntimeException(sprintf(
//                        'Cannot update User with identifier %d; does not exist', $userId
//                ));
//            }
//            $this->tableGateway->update($data, ['id' => $userId]);
        }
    }
    
    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getCountries(){
        $sqlQuery = "SELECT * FROM country";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }
    
    /**
     * 
     * @return unknown
     */
    public function getAgencies(){
        $sqlQuery = "SELECT * FROM agencies where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }
    
    /**
     * 
     * @return unknown
     */
    public function getServices(){
        $sqlQuery = "SELECT * FROM services where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }
    
    /**
     * 
     * @return unknown
     */
    public function getGrades(){
        $sqlQuery = "SELECT * FROM staff_grades where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }
    
    /**
     * 
     * @param unknown $stockID
     */
    public function getProcessSteps( $stockID = NULL ){
        if(!empty($stockID)){
            $sqlQuery = "SELECT users_survey_processcost.*, users_survey.country_id, staff_grades.name as grade_name FROM users_survey_processcost LEFT JOIN users_survey ON users_survey_processcost.survey_id = users_survey.id LEFT JOIN staff_grades ON staff_grades.id = users_survey_processcost.grade_level where users_survey_processcost.stock_id = ".$stockID." order by users_survey_processcost.last_modified desc";
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return $resultSet;
        }
    }
    
    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function saveProcessSteps($data = []) {
        if (!empty($data)) {
            $this->tableGateway->insert($data);
            return true;
        }
    }
    
    /**
     * @param unknown $surveyID
     * @param unknown $stockID
     * @param unknown $gradeLevel
     * @return array
     */
    public function checkGradeCost($surveyID = NULL, $stockID = NULL, $gradeLevel = NULL){
        $resultSet = [];
        if(!empty($surveyID) && !empty($stockID) && !empty($gradeLevel)){
            $sqlQuery = "SELECT * FROM users_survey_processcost where survey_id = ".$surveyID." AND stock_id = ".$stockID." AND grade_level = ".$gradeLevel;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
        } 
        return $resultSet;
    }
    
    /**
     * 
     * @param unknown $parentID
     * @return unknown
     */
    public function getChildServices( $parentID = NULL ){
        $sqlQuery = "SELECT * FROM services_childs where parent_id = ".$parentID;
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }

    /**
     * @param unknown $serviceID
     * @param unknown $serviceChildID
     */
    public function getBaselineSurveyByServices($serviceID = NULL, $serviceChildID = NULL, $customerID = NULL){
        if( $serviceID > 0 && $serviceChildID > 0 && $customerID > 0 ){
            $sqlQuery = "SELECT users_survey.country_id, users_survey.agency_id, users_survey_stocktaking.* FROM users_survey
LEFT JOIN users_survey_stocktaking ON users_survey_stocktaking.survey_id = users_survey.id
WHERE users_survey.user_id = ".$customerID." AND users_survey_stocktaking.service_id = ".$serviceID." AND users_survey_stocktaking.service_child_id = ".$serviceChildID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            $resultSet = isset($resultSet[0]) ? $resultSet[0] : $resultSet;
            return $resultSet;
        }
    }
    
    /**
     * @param unknown $costID
     */
    public function removeCost($costID = NULL){
        if($costID > 0){
            return $this->tableGateway->delete(array('id'=>$costID));
        }
    }

}
