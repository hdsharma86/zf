<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Consultant;

class Module
{
    const VERSION = '3.0.2dev';

    /**
     * [onBootstrap description]
     * @param  [type] $e [description]
     * @return [type]    [description]
     */
    public function onBootstrap($e)
    {
    	
    }
    
    /**
     * [getConfig description]
     * @return [type] [description]
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * [getSettings description]
     * @return [type] [description]
     */
    public function getSettings(){

    }
    
}
