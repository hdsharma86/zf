<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction(){
        return new ViewModel();
    }
    
    /**
     * Foumulla : (Number of Transaction of Processes) * (Unit Transaction Cost)
     * @param number $numberOfTxns : Number of Transaction of Processes
     * @param number $unitTxnCost : Unit Transaction Cost
     * @return number
     */
    public static function getBaselineTotalTransactionCost( $numberOfTxns = 0, $unitTxnCost = 0 ){
        return $numberOfTxns * $unitTxnCost;
    }
    
    /**
     * Returned from : getBaselineTotalTransactionCost()
     * Formulla : (Total amount ($) invoiced by the Supplier during last year) + (TOTAL TRANSACTION COST (Staff time))
     * @param number $baselineTotalAmt
     * @param number $totalTxnCost
     */
    public static function getBaselineTotalCost( $baselineTotalAmt = 0, $totalTxnCost = 0 ){
        return $baselineTotalAmt + $totalTxnCost;
    }
    
    /**
     * @param number $unitSaving
     * @param number $numberOfTxns
     * @return number
     */
    public static function getBaselineTotalSavingOnTC( $unitSaving = 0, $numberOfTxns = 0 ){
        return $baselineTotalAmt * $totalTxnCost;
    }
    
    /**
     * Calculate & return 
     * @param unknown $annualCost
     * @return number
     */
    public static function getGradeMonthlyCost( $annualCost = NULL ){
        $monthCost = 0;
        if(!empty($annualCost) && $annualCost > 0){
            $monthCost = $annualCost / 12;
        }
        return $monthCost;
    }
    
    /**
     * @param unknown $annualCost
     * @return number
     */
    public static function getGradeDailyCost( $annualCost = NULL ){
        $dailyCost = 0;
        if(!empty($annualCost) && $annualCost > 0){
            $dailyCost = $annualCost / 21.75;
        }
        return $dailyCost;
    }
    
    /**
     * @param unknown $annualCost
     * @return number
     */
    public static function getGradeHourlyCost( $annualCost = NULL ){
        $hourlyCost = 0;
        if(!empty($annualCost) && $annualCost > 0){
            $hourlyCost = $annualCost / 7.5;
        }
        return $hourlyCost;
    }
}
