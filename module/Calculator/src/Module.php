<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator;

class Module
{
    const VERSION = '3.0.2dev';

    /**
     * [onBootstrap description]
     * @param  [type] $e [description]
     * @return [type]    [description]
     */
    public function onBootstrap($e)
    {
    	$e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractController', 'dispatch', function($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');

            if(isset($config['constants']) && !empty($config['constants'])){
                foreach ($config['constants'] as $const => $value) {
                    if(!empty($const) && !empty($value)){
                        !defined($const) ? define($const, $value) : '';
                    }
                }
            }

            if (isset($config['module_layouts'][$moduleNamespace])) {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }

        }, 100);
        
        $serviceManager = $e->getApplication()->getServiceManager();
    }
    
    /**
     * [getConfig description]
     * @return [type] [description]
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * [getSettings description]
     * @return [type] [description]
     */
    public function getSettings(){

    }
    
}
