<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'customer-login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-login',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'login',
                    ],
                ]
            ],
            'customer-logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-logout',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'logout',
                    ],
                ]
            ],
            'customer-dashboard' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-dashboard',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'dashboard',
                    ],
                ]
            ],
            'forgot-password' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/forgot-password',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'forgot',
                    ],
                ]
            ],
            'customer-registration' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-registration',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'register',
                    ],
                ]
            ],
            'customer-thankyou' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-thankyou',
                    'defaults' => [
                        'controller' => Controller\CustomerController::class,
                        'action'     => 'thanks',
                    ],
                ]
            ],
            'customer-survey' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customer-survey',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action'     => 'index',
                    ],
                ]
            ],
            'baseline-survey' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/baseline-survey',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action'     => 'baseline',
                    ],
                ]
            ],
            'process-survey-processcost' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/process-survey-processcost',
                    'defaults' => [
                        'controller' => Controller\SurveyController::class,
                        'action'     => 'processcost',
                    ],
                ]
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Factory\AppControllerFactory::class,
            Controller\AjaxController::class => Factory\AppControllerFactory::class,
            Controller\SurveyController::class => Factory\AppControllerFactory::class,
            Controller\CustomerController::class => Factory\AppControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Model\SurveyTable::class => InvokableFactory::class,
            Model\CustomerTable::class => InvokableFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\SetSelectOptionsHelper::class => InvokableFactory::class,
        ],
        'aliases' => [
            'setSelectOptions' => View\Helper\SetSelectOptionsHelper::class
        ]
    ],   
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ],
    ],
];
