<?php

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class SurveyTable {

    private $tableGateway;

    /**
     * [__construct description]
     * @param TableGatewayInterface $tableGateway [description]
     */
    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }

    public function saveBaselineSurvey($data = array()) {
        
        if (!empty($data)) {
            //$userId = (int) $data['id'];
            $this->tableGateway->insert($data);
            return true;
            //if (!$this->getUser($userId)) {
            //    throw new RuntimeException(sprintf(
            //          'Cannot update User with identifier %d; does not exist', $userId
            //    ));
            //}
            //$this->tableGateway->update($data, ['id' => $userId]);
        }
    }
    
    /**
     * @param unknown $surveyID
     * @param array $data
     */
    public function updateBaselineSurvey($surveyID = NULL, $data = []){
        
        if(!empty($data) && $surveyID > 0){
            $userSurveyStocktakingTable = new TableGateway('users_survey_stocktaking', $this->dbAdapter);
            $userSurveyStocktakingTable->update($data, ['id' => $surveyID]);
            return true;
        } else {
            throw new RuntimeException(sprintf(
               'Invalid or Empty identity supplied: %d', $surveyID
            ));
        }
        
    }

    /**
     * [getCountries description]
     * @return [type] [description]
     */
    public function getCountries(){
        $sqlQuery = "SELECT * FROM country";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }
    
    /**
     * 
     * @return unknown
     */
    public function getAgencies(){
        $sqlQuery = "SELECT * FROM agencies where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet; 
    }
    
    /**
     * 
     * @return unknown
     */
    public function getServices(){
        $sqlQuery = "SELECT * FROM services where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }

    /**
     * @return array|\Zend\Db\Adapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
     */
    public function getGrades(){
        $sqlQuery = "SELECT * FROM staff_grades where status = 1";
        $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
        $resultSet = $resultSet->toArray();
        return $resultSet;
    }

    /**
     * 
     * @param unknown $customerID
     * @throws RuntimeException
     * @return unknown
     */
    public function hasSurvey( $customerID = NULL ){
        if(!empty($customerID) && $customerID > 0){
            $sqlQuery = "SELECT * FROM users_survey where user_id = ".$customerID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(1));
            $resultSet = $resultSet->toArray();
            $resultSet = isset($resultSet[0]) ? $resultSet[0] : $resultSet;
            return $resultSet;
        } else {
            throw new RuntimeException(sprintf(
                'Invalid identity supplied ', $customerID
            ));
        }
    }
    
    /**
     * @param array $data
     */
    public function saveSurvey( $data = [] ){
        if(!empty($data)){
            $usersSurvey = new TableGateway('users_survey', $this->dbAdapter);
            $usersSurvey->insert($data);
            return true;
        }
    }
    
    /**
     * @param unknown $customerID
     */
    public function getBaselineSurvey( $customerID = NULL ){
        if(!empty($customerID) && $customerID > 0){
            $sqlQuery = "SELECT users_survey.user_id, users_survey.country_id, users_survey.agency_id, users_survey_stocktaking.*, country.name as country_name, services.title as service_area, agencies.title as agency_name, services_childs.title as existing_service FROM users_survey
RIGHT JOIN users_survey_stocktaking ON users_survey.id = users_survey_stocktaking.survey_id
LEFT JOIN country ON country.id = users_survey.country_id
LEFT JOIN services ON services.id = users_survey_stocktaking.service_id
LEFT JOIN agencies ON agencies.id = users_survey.agency_id
LEFT JOIN services_childs ON services_childs.id = users_survey_stocktaking.service_child_id
WHERE users_survey.user_id = ".$customerID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return $resultSet;
        }
    }
    
    /**
     * @param unknown $serviceID
     * @param unknown $serviceChildID
     */
    public function getBaselineSurveyByServices($serviceID = NULL, $serviceChildID = NULL, $customerID = NULL){
        if( $serviceID > 0 && $serviceChildID > 0 && $customerID > 0 ){
            $sqlQuery = "SELECT users_survey.country_id, users_survey.agency_id, users_survey_stocktaking.* FROM users_survey
LEFT JOIN users_survey_stocktaking ON users_survey_stocktaking.survey_id = users_survey.id
WHERE users_survey.user_id = ".$customerID." AND users_survey_stocktaking.service_id = ".$serviceID." AND users_survey_stocktaking.service_child_id = ".$serviceChildID;
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return $resultSet;
        }
    }

    /**
     * @param null $customerID
     * @param null $surveyID
     * @return array
     */
    public function getAdditionalKeyInformation($customerID = NULL, $surveyID = NULL){
        if(!empty($customerID) && !empty($surveyID)){
            $sqlQuery = "SELECT un_agencies,number_of_LTAs,undaf_cycle,number_of_OMTs,service_manager FROM `users_survey_keys_info` WHERE user_id = {$customerID} AND survey_id = {$surveyID}";
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return isset($resultSet[0])?$resultSet[0]:$resultSet;
        }
    }

    /**
     * @param null $customerID
     * @param null $serviceID
     * @param null $existingServiceID
     * @return array
     */
    public function getStockID( $surveyID = NULL, $serviceID = NULL, $existingServiceID = NULL ){
        if(!empty($customerID) && !empty($serviceID) && !empty($existingServiceID)){
            $sqlQuery = "SELECT id, survey_id, service_id, service_child_id FROM `users_survey_stocktaking` WHERE survey_id = {$surveyID} AND service_id = {$serviceID} AND service_child_id = {$existingServiceID}";
            $resultSet = $this->dbAdapter->query($sqlQuery, array(5));
            $resultSet = $resultSet->toArray();
            return isset($resultSet[0])?$resultSet[0]:$resultSet;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function saveProcessStep( $data = [] ){
        if(!empty($data)){
            $usersSurvey = new TableGateway('users_survey_processcost', $this->dbAdapter);
            $usersSurvey->insert($data);
            return true;
        }
    }

    /**
     * @param $table
     * @param array $data
     * @param string $where
     * @return bool
     */
    public function update($table, $data = [], $where = []){

        if(!empty($table) && !empty($data) && $where){
            $tableGateway = new TableGateway($table, $this->dbAdapter);
            $tableGateway->update($data, $where);
            return true;
        } else {
            throw new RuntimeException(sprintf(
                'Invalid or Empty identity supplied'
            ));
        }

    }

    /**
     * @param string $table
     * @param array $data
     * @return int
     */
    public function save( $table = '', $data = [] ){
        if(!empty($table) && !empty($data)){
            $tableGateway = new TableGateway($table, $this->dbAdapter);
            $tableGateway->insert($data);
            return $tableGateway->getLastInsertValue();
        }
    }

    /**
     * @param string $table
     * @param array $where
     * @return mixed
     */
    public function fetchOne( $table = '', $where = [] ){
        $tableGateway = new TableGateway($table, $this->dbAdapter);
        $rowSet = $this->tableGateway->select(function (\Zend\Db\Sql\Select $select) use ($where)  {
            $select->columns(array('*'))
                   ->where($where)
                   ->limit(1);
        });
        return $rowSet->current();
    }

    /**
     * @param string $table
     * @param array $where
     * @return mixed
     */
    public function fetchAll( $table = '', $where = [] ){
        $tableGateway = new TableGateway($table, $this->dbAdapter);
        $result = $this->tableGateway->select(function (Select $select) use ($where)  {
            $select->where($where)
                   ->order('id ASC');
        });
        return $result->toArray();
    }

}
