<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Model\SurveyTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Application\Form\BaselineForm;
use Application\Form\Filter\BaselineFilter;
use Common\Controller\CommonController as COMMON_UTILITY;

class SurveyController extends AbstractActionController {

    protected $dbAdapter;
    protected $authAdapter;
    protected $authService;
    
    protected $procServices = [
        '' => '--Select Common Procurement--',
        'Office supplies' => 'Office supplies',
        'LTAs for workshop and conference facilities, standard business supplies, etc.' => 'LTAs for workshop and conference facilities, standard business supplies, etc.',
        'Joint Procurement Review Committee procurement' => 'Joint Procurement Review Committee procurement',
        'LTA database (UNGM)' => 'LTA database (UNGM)',
        'Local insurance' => 'Local insurance',
        'Management and evaluation consultancy services' => 'Management and evaluation consultancy services',
        'Other' => 'Other'
    ];
    
    protected $baselineServiceQuality = [
        ''  => '-Select Service Quality-',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5'
    ];
    
    protected $targetCostSave = [
        '' => '--Select Cost--',
        '5' => '5% of baseline',
        '10' => '10% of baseline',
        '15' => '15% of baseline',
        '20' => '20% of baseline',
        'Other' => 'Other',
    ];
    
    protected $targetEfficiencyGain = [
        '' => '--Select Efficiency Gains--',
        '5' => '5% of baseline',
        '10' => '10% of baseline',
        '15' => '15% of baseline',
        '20' => '20% of baseline',
        'Other' => 'Other'
    ];
    
    protected $targetServiceQuality = [
        '' => '--Select Ranking--',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
    ];
    
    protected $assessmentAction = [
        '' => '--Select Recommended Action--',
        'CONTINUE AS IS' => 'CONTINUE AS IS',
        'SCALE UP' => 'SCALE UP',
        'SCALE DOWN' => 'SCALE DOWN',
        'DISCONTINUE' => 'DISCONTINUE',
        'MODIFY' => 'MODIFY',
    ];
    
    protected $yearRange = [
        'START' => 0,
        'OFFSET' => 100
    ];
    
    protected $modality = [
        'Outsources' => 'Outsources',
        'In-house' => 'In-house'
    ];
    
    public function __construct(Adapter $dbAdapter, AuthenticationService $authService) {
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @param \Zend\Mvc\MvcEvent $e
     * @return mixed|\Zend\Http\Response
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/customer-login');
        }
        
        return parent::onDispatch($e);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function indexAction() {

        $customerDetail = [];
        if($this->authService->hasIdentity()){
            $customerDetail = $this->authService->getIdentity();
        }
        
        $surveyDetail = $this->getModel()->getBaselineSurvey($customerDetail['id']);
                
        $customerHasSurvey = $this->getModel()->hasSurvey($customerDetail['id']);
        //Getting additional key information....
        if(!empty($customerHasSurvey)){
            $additionalKeyInformation = $this->getModel()->getAdditionalKeyInformation($customerDetail['id'], $customerHasSurvey['id']);
        }

        $request = $this->getRequest();
        if($request->isPost()){
            $data = $request->getPost();
            
            $surveyArray = [];
            if(!empty($data)){
                
                if(empty($customerHasSurvey)){
                    $this->getModel()->saveSurvey(array(
                        'user_id'   => $customerDetail['id'],
                        'country_id' => $data['baseline_country'],
                        'agency_id' => $data['baseline_agency'],
                        'last_modified' => date('Y-m-d H:i:s'),
                        'created_on' => date('Y-m-d H:i:s'),
                    ));
                    $customerHasSurvey = $this->getModel()->hasSurvey($customerDetail['id']);
                }
                
                $surveyArray = [
                    'baseline_year' => $data['baseline_year'],
                    'baseline_cost' => $data['baseline_year_cost'],
                    'baseline_quantity_purchased' => $data['baseline_qty_purchased'],
                    'baseline_service_quality' => $data['baseline_year_service_quality'],
                    'target_year' => $data['target_year'],
                    'target_cost_save' => $data['target_year_cost'],
                    'target_efficiency_gain' => $data['target_year_efficiency_gain'],
                    'target_service_quality' => $data['target_year_service_quality'],
                    'managing_entity' => $data['managing_entity'],
                    'modality' => $data['modality'],
                    'modality_type' => $data['madality_type'],
                    'assessment_action' => $data['recommended_action'],
                    'additional_comment' => $data['additional_comment'],
                    'last_modified' => date('Y-m-d H:i:s')
                ];
                
                if(!empty($data['clients'])){
                    $surveyArray['clients'] = implode(',', $data['clients']);
                }
                
                if(isset($data['survey_id']) && !empty($data['survey_id'])){
                    $this->getModel()->updateBaselineSurvey($data['survey_id'], $surveyArray);
                } else {
                    $surveyArray['survey_id'] = $customerHasSurvey['id'];
                    $surveyArray['service_id'] = $data['baseline_common_service'];
                    $surveyArray['service_child_id'] = $data['baseline_existing_service'];
                    $surveyArray['created_on'] = date('Y-m-d H:i:s');
                    $this->getModel()->saveBaselineSurvey($surveyArray);
                }
                
                $this->flashMessenger()->addMessage(array('success' => 'Thanks, Your input to survey has been submitted successfully, please proceed to next level.'));
                return $this->redirect()->tourl('customer-survey');
                
            }
        }
        
        $range = [];
        $this->yearRange['START'] = date('Y');
        $this->yearRange['START'] += 10;
        for( $i=$this->yearRange['START']; $i>=($this->yearRange['START']-$this->yearRange['OFFSET']); $i-- ){
            $range[$i] = $i;
        }

        $view = new ViewModel();
        $viewData = [
            'countries'                 => COMMON_UTILITY::arrayToList($this->getModel()->getCountries(),'id','name'),
            'agencies'                  => COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'id','title'),
            'services'                  => COMMON_UTILITY::arrayToList($this->getModel()->getServices(),'id','title'),
            'grades'                    => COMMON_UTILITY::arrayToList($this->getModel()->getGrades(),'id','name', '-Select Grade Level-'),
            'managingEntity'            => COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title'),
            'clients'                   => COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title'),
            'baselineServiceQuality'    => $this->baselineServiceQuality,
            'targetCostSave'            => $this->targetCostSave,
            'targetEfficiencyGain'      => $this->targetEfficiencyGain,
            'targetServiceQuality'      => $this->targetServiceQuality,
            'assessmentAction'          => $this->assessmentAction,
            'yearRange'                 => $range,
            'modality'                  => $this->modality,
            'customer_id'               => $customerDetail['id'],
            'additional_key_info'       => $additionalKeyInformation,
            'survey_id'                 => $customerHasSurvey['id']
        ];
        /*$view->setVariable('countries', COMMON_UTILITY::arrayToList($this->getModel()->getCountries(),'id','name'));
        $view->setVariable('agencies', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'id','title'));
        $view->setVariable('services', COMMON_UTILITY::arrayToList($this->getModel()->getServices(),'id','title'));
        $view->setVariable('managingEntity', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title'));
        $view->setVariable('clients', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title'));
        $view->setVariable('baselineServiceQuality', $this->baselineServiceQuality);
        $view->setVariable('targetCostSave', $this->targetCostSave);
        $view->setVariable('targetEfficiencyGain', $this->targetEfficiencyGain);
        $view->setVariable('targetServiceQuality', $this->targetServiceQuality);
        $view->setVariable('assessmentAction', $this->assessmentAction);
        $view->setVariable('yearRange', $range);
        $view->setVariable('modality', $this->modality);
        $view->setVariable('customer_id', $customerDetail['id']);*/
        $view->setVariable('data', $viewData);
        return $view;
    }

    public function processcostAction(){
        $request = $this->getRequest();
        $response = ['status'=>false,'message'=>'Something went wrong.'];
        if ($request->isPost()) {
            $data = $request->getPost();
            $customerID = $data['customer_id'];
            $surveyID = $data['survey_id'];
            $serviceID = $data['processcost_common_service'];
            $existingServiceID = $data['processcost_existing_service'];
            if(!empty($data)){

                //$stockID = $this->getModel()->getStockID($surveyID, $serviceID, $existingServiceID);
                $stockID = $this->getModel()->fetchOne('users_survey_stocktaking', ['survey_id'=>$surveyID, 'service_id'=>$serviceID, 'service_child_id'=>$existingServiceID]);
                if(!empty($stockID)){
                    $processData = [
                        'user_id'       => $customerID,
                        'survey_id'     => $surveyID,
                        'stock_id'      => $stockID['id'],
                        'has_LTA'       => 0,
                        'process_step'  => $data['process_step'],
                        'staff_involved'=> $data['staff_involved'],
                        'grade_level'   => $data['grade_level'],
                        'time_required' => $data['time_required'],
                        'last_modified' => date('Y-m-d H:i:s'),
                        'created_on'    => date('Y-m-d H:i:s')
                    ];
                    $this->getModel()->saveProcessStep($processData);
                    $response = ['status'=>true, 'message'=>'Process step has been saved successfully.'];
                } else {
                    $response = ['status'=>false, 'message'=>'Please enter submit your baseline analysis for the current service before proceed to input process steps.'];
                }

                $isExistsKeyInfo = $this->getModel()->fetchOne('users_survey_keys_info', ['survey_id'=>$surveyID]);
                if(!empty($isExistsKeyInfo)){
                    $keyInfoData = [
                        'user_id'           => $customerID,
                        'survey_id'         => $surveyID,
                        'un_agencies'       => $data['un_agencies'],
                        'number_of_LTAs'    => $data['ltas_developed'],
                        'undaf_cycle'       => $data['UNDAF_cycle'],
                        'number_of_OMTs'    => $data['omt_groups'],
                        'service_manager'   => $data['service_manager_grade'],
                        'last_modified'     => date('Y-m-d H:i:s'),
                        'created_on'        => date('Y-m-d H:i:s'),
                    ];
                    $this->getModel()->save('users_survey_keys_info',$keyInfoData);
                } else {
                    $keyInfoData = [
                        'un_agencies'       => $data['un_agencies'],
                        'number_of_LTAs'    => $data['ltas_developed'],
                        'undaf_cycle'       => $data['UNDAF_cycle'],
                        'number_of_OMTs'    => $data['omt_groups'],
                        'service_manager'   => $data['service_manager_grade'],
                        'last_modified'     => date('Y-m-d H:i:s')
                    ];
                    $this->getModel()->update('users_survey_keys_info', $keyInfoData, $isExistsKeyInfo['id']);
                }
            }
        }
        echo json_encode($response);
        die;
    }

    /**
     * @return ViewModel
     */
    public function baselineAction() {

        $request = $this->getRequest();
        $customerDetail = [];
        if($this->authService->hasIdentity()){
            $customerDetail = $this->authService->getIdentity();
        }
        
        $baselineSurveyFrom = new BaselineForm('baseline_survey');
        $baselineSurveyFrom->setInputFilter(new BaselineFilter());

        if ($request->isPost()) {

            $formData = $request->getPost();
            $baselineSurveyFrom->setData($formData);

            if ($baselineSurveyFrom->isValid()) {
                
                $data = (array)$formData;
                
                $data['user_id'] = $customerDetail['id'];
                if($data['proc_service'] == 'Other'){
                    $data['proc_service'] = $data['other_proc_service'];
                    unset($data['other_proc_service']);
                }
                
                if($data['target_cost_save'] == 'Other'){
                    $data['target_cost_save'] = $data['other_target_cost_save'];
                    unset($data['other_target_cost_save']);
                }
                
                if($data['target_efficiency_gain'] == 'Other'){
                    $data['target_efficiency_gain'] = $data['other_target_efficiency_gain'];
                    unset($data['other_target_efficiency_gain']);
                }
                
                if(!empty($data['clients'])){
                    $data['clients'] = implode(',', $data['clients']);
                }
                $data['last_modified'] = date('Y-m-d H:i:s');
                $data['created_on'] = date('Y-m-d H:i:s');
                
                unset($data['submit']);
               
                $this->getModel()->saveSurveyForm($data);
                $this->flashMessenger()->addMessage(array('success' => 'Thanks, Your survey has been completed successfully.'));
                return $this->redirect()->tourl('baseline-survey');
                
            } else {
                $errors = $baselineSurveyFrom->getMessages();
            }
            
        }

        $baselineSurveyFrom->get('country_id')->setAttribute('options', COMMON_UTILITY::arrayToList($this->getModel()->getCountries(),'id','name','-Select Country-'));
        $baselineSurveyFrom->get('agency_id')->setAttribute('options', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'id','title','-Select Agency-'));
        $baselineSurveyFrom->get('service_id')->setAttribute('options', COMMON_UTILITY::arrayToList($this->getModel()->getServices(),'id','title','-Select Service-'));
        $baselineSurveyFrom->get('proc_service')->setAttribute('options', $this->procServices);
        $baselineSurveyFrom->get('managing_entity')->setAttribute('options', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title','-Select Managing Agency-'));
        $baselineSurveyFrom->get('clients')->setAttribute('options', COMMON_UTILITY::arrayToList($this->getModel()->getAgencies(),'title','title','-Select Clients-'));
        $baselineSurveyFrom->get('baseline_service_quality')->setAttribute('options', $this->baselineServiceQuality);        
        $baselineSurveyFrom->get('target_cost_save')->setAttribute('options', $this->targetCostSave);
        $baselineSurveyFrom->get('target_efficiency_gain')->setAttribute('options', $this->targetEfficiencyGain);
        $baselineSurveyFrom->get('target_service_quality')->setAttribute('options', $this->targetServiceQuality);
        $baselineSurveyFrom->get('assessment_action')->setAttribute('options', $this->assessmentAction);
        
        $view = new ViewModel();
        $view->setVariable('baseline_survey_form', $baselineSurveyFrom);
        return $view;
    }

    /**
     * @return SurveyTable
     */
    private function getModel() {
        $tableGateway = new TableGateway('users_survey_stocktaking', $this->dbAdapter);
        return new SurveyTable($tableGateway);
    }

}
