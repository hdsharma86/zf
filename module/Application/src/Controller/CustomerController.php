<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Model\CustomerTable;
use Zend\View\Model\ViewModel;
use Zend\Db\TableGateway\TableGateway;
use Application\Form\CustomerRegistrationForm;
use Application\Form\Filter\CustomerRegisterFilter;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Application\Form\CustomerForgotPassForm;
use Application\Form\CustomerLoginForm;
use Application\Form\Filter\CustomerLoginFilter;
use Application\Form\Filter\CustomerForgotPassFilter;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;
use Common\Controller\CommonController as COMMON;

class CustomerController extends AbstractActionController {

    protected $dbAdapter;
    protected $authAdapter;
    protected $authService;

    public function __construct(Adapter $dbAdapter, AuthenticationService $authService) {
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;
    }

    public function indexAction() {
        return new ViewModel();
    }

    /**
     * [baselineAction description]
     * @return [type] [description]
     */
    public function registerAction() {
        
        if ($this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/customer-dashboard');
        }
        
        $request = $this->getRequest();

        $customerRegistrationForm = new CustomerRegistrationForm('customer_registration');
        $customerRegistrationForm->setInputFilter(new CustomerRegisterFilter());

        if ($request->isPost()) {

            $formData = $request->getPost();
            $customerRegistrationForm->setData($formData);

            if ($customerRegistrationForm->isValid()) {
                $data = $customerRegistrationForm->getData();
                $password = COMMON::getPassword();
                $userData = array(
                    'type_id'   => '3',
                    'email' => $data['email'],
                    'username' => $data['email'],
                    'password' => md5($password),
                    'is_active' => '0',
                    'is_deleted' => '0',
                    'last_modified' => date('Y-m-d H:i:s'),
                    'created_on' => date('Y-m-d H:i:s')
                );
                $profileData = array(
                    'first_name' => $data['first_name'],
                    'middle_name' => $data['middle_name'],
                    'last_name' => $data['last_name'],
                    'contact_type' => $data['phone_type'],
                    'intl_prefix' => $data['int_prefix'],
                    'contact_number' => $data['contact_number'],
                    'address'   => $data['address'],
                    'country'   => $data['country'],
                    'state' => $data['state'],
                    'city' => $data['city'],
                    'zip' => $data['postal_code'],                    
                    'last_modified' => date('Y-m-d H:i:s'),
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->getModel()->saveCustomers($userData, $profileData);
                
                $bodyContent = "<p>Your registration has been successfully completed with Appleton, your account has still been under verification mode. Please use following credentials for login:</p><br /><br />";
                $bodyContent .= "Username : ".$data['email']."<br />";
                $bodyContent .= "Password : ".$password." <br /><br />";
                $bodyContent .= "Thanks<br />Appleton Consulting Corporation";

                COMMON::sendMail(array(
                    'TO' => $data['email'],
                    'FROM' => 'admin@boms.com',
                    'SUBJECT' => 'Thanks for being part of BoMS.',
                    'CONTENT' => $bodyContent
                ));

                return $this->redirect()->tourl('customer-thankyou');
            } else {
                $errors = $customerRegistrationForm->getMessages();
            }
        }

        $countries = $this->getModel()->getCountries();
        $states = $this->getModel()->getStates();

        $countryList = array();
        if(!empty($countries)){
            $countryList[''] = '-Select Country-';
            foreach ($countries as $country) {
                $countryList[$country['id']] = $country['name'];
            }
        }

        $stateList = array();
        if(!empty($states)){
            foreach ($states as $state) {
                $stateList[$state['id']] = $state['name'];
            }
        }

        $customerRegistrationForm->get('country')->setAttribute('options',$countryList);
        //$customerRegistrationForm->get('state')->setAttribute('options' ,$stateList);

        $this->layout()->setTemplate('layout/register');
        $view = new ViewModel();
        $view->setVariable('register_form', $customerRegistrationForm);
        return $view;
    }

 	public function logoutAction(){
    	$this->authService->clearIdentity();
    	$this->flashMessenger()->addMessage(array('success' => 'Logout Successfully.'));
    	return $this->redirect()->toUrl('/customer-login');
    }

    public function dashboardAction() {

        if (!$this->authService->hasIdentity()) {
            return $this->redirect()->tourl('/customer-login');
        }

        $view = new ViewModel();
        return $view;
    }

    public function forgotAction() {

        $request = $this->getRequest();

        $CustomerForgotPassForm = new CustomerForgotPassForm('forgot_password');
        $CustomerForgotPassForm->setInputFilter(new CustomerForgotPassFilter());
        if ($request->isPost()) {

            $formData = $request->getPost();
            $CustomerForgotPassForm->setData($formData);
            if ($CustomerForgotPassForm->isValid()) {
                $password = COMMON::getPassword();
                $data = $CustomerForgotPassForm->getData();
                $email = $data['email'];
                $is_email = $this->getModel()->checkCustomers($email);
                if (count($is_email) > 0) {
                    $this->getModel()->changePassword($email, md5($password));
                   $bodyContent = "<p>Your password has been successfully changed. Please use following credentials for login:</p><br />";
                   $bodyContent .= "Email : " . $email . " <br />";
                   $bodyContent .= "Password : " . $password . " <br /><br />";
                   $bodyContent .= "Thanks<br />Appleton Consulting Corporation";

                   COMMON::sendMail(array(
                       'TO' => $data['email'],
                       'FROM' => 'admin@boms.com',
                       'SUBJECT' => 'BoMS - Password Reset.',
                       'CONTENT' => $bodyContent
                   ));
                    $this->flashMessenger()->addMessage(array('success' => 'Password sent to your email Successfully.'));
                } else {
                    $this->flashMessenger()->addMessage(array('error' => 'Email is not found.'));
                }
                return $this->redirect()->tourl('forgot-password');
            }
        }
        $this->layout()->setTemplate('layout/customer_login');
        $view = new ViewModel();
        $view->setVariable('forgot_password', $CustomerForgotPassForm);
        return $view;
    }

    /**
     * @return unknown|\Zend\View\Model\ViewModel
     */
    public function loginAction() {

        $request = $this->getRequest();

        $customerLoginForm = new CustomerLoginForm('customer_login');
        $customerLoginForm->setInputFilter(new CustomerLoginFilter());
        if ($request->isPost()) {

            $formData = $request->getPost();
            $customerLoginForm->setData($formData);
            if ($customerLoginForm->isValid()) {
                $data = $customerLoginForm->getData();
                $email = $data['email'];
                $userPassword = MD5($data['password']);

                $this->authAdapter = new AuthAdapter($this->dbAdapter);
                $this->authAdapter->setTableName('users')->setIdentityColumn('email')->setCredentialColumn('password');
                $this->authAdapter->setIdentity($email);
                $this->authAdapter->setCredential($userPassword);
                $this->authAdapter->getDbSelect()->where('type_id = 3');
                $this->authAdapter->getDbSelect()->where('is_active = 1');
                $this->authAdapter->getDbSelect()->where('is_deleted = 0');

                $result = $this->authService->authenticate($this->authAdapter);
                if ($result->isValid()) {
                    $resultRow = $this->authAdapter->getResultRowObject();
                    $this->authService->getStorage()->write(array(
                        'id'          => $resultRow->id,
                        'type_id'     => $resultRow->type_id,
                        'email'       => $resultRow->email,
                        'username'    => $resultRow->username,
                        'is_active'   => $resultRow->is_active,
                        'user_agent'  => $request->getServer('HTTP_USER_AGENT'))
                    );
                   
                    $this->flashMessenger()->addMessage(array('success' => 'Login Success.'));
                     return $this->redirect()->tourl('/customer-dashboard');
                } else {
                    $this->flashMessenger()->addMessage(array('error' => 'invalid credentials.'));
                }
                return $this->redirect()->tourl('/customer-login');
                
            } else {
                $errors = $customerLoginForm->getMessages();
            }
        }

        $this->layout()->setTemplate('layout/customer_login');
        $view = new ViewModel();
        $view->setVariable('customer_login', $customerLoginForm);
        return $view;
        
    }

    /**
     * [thanksAction description] customer-thankyou
     * @return [type] [description]
     */
    public function thanksAction(){
        $view = new ViewModel();
        return $view;
    }

    /**
     * Function to get required model
     * @return \Application\Model\CustomerTable
     */
    private function getModel() {
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new CustomerTable($tableGateway);
    }

}
