<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Model\CustomerTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;


class AjaxController extends AbstractActionController
{
	protected $dbAdapter;
    protected $authAdapter;
    protected $authService;

    public function __construct(Adapter $dbAdapter, AuthenticationService $authService) {
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function indexAction()
    {
    	$response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
        	$requestData = $request->getPost();
        	if(!empty($requestData)){
        		switch ($requestData['action']) {
				    case 'states_by_country':
				        $response = $this->getStatesByCountry($requestData);
				        break;
				    default;
				} 
        		if($requestData['action'] == 'states_by_country'){
        			$response = $this->getStatesByCountry($requestData);
        		}
        	}
        }

        $jsonModel = new JsonModel();
        $jsonModel->setTerminal(true);
        return new $jsonModel($response);
    }

    /**
     * [getStatesByCountry description]
     * @return [type] [description]
     */
    private function getStatesByCountry($requestData){

    	$return = [];
        $countryID = $requestData['countryId'];
    	$states = $this->getModel()->getStatesByCountry($countryID);
        $stateArr = [];
    	if(!empty($states)){
    		foreach($states as $state){
    			$stateArr[$state['id']] = $state['name'];
    		}
    	}
        $return['states'] = $stateArr;
        
        $countryPhoneCode = $this->getModel()->getCountryPhoneCode($countryID);
        if(!empty($countryPhoneCode)){
            $return['country_phone_code'] = $countryPhoneCode['phonecode'];
        }

    	return $return;

    }

    /**
     * [getModel description]
     * @return [type] [description]
     */
    private function getModel() {
        $tableGateway = new TableGateway('users', $this->dbAdapter);
        return new CustomerTable($tableGateway);
    }

}
