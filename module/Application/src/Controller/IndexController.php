<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Authentication\AuthenticationService;
use Common\Controller\CommonController as COMMON;

class IndexController extends AbstractActionController
{

    public function __construct(Adapter $dbAdapter, AuthenticationService $authService) {
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;
    }
    
    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if ($this->authService->hasIdentity()) {
            $customerDetail = $this->authService->getIdentity();
            if($customerDetail['type_id'] == 3){
                return $this->redirect()->tourl('/customer-dashboard');
            }
        }
        
        return parent::onDispatch($e);
    }
    
    public function indexAction()
    {
        return new ViewModel();
    }

    public function loginAction()
    {
    	$this->layout()->setTemplate('layout/customer_login');
        return new ViewModel();
    }

    public function registerAction()
    {
    	$this->layout()->setTemplate('layout/register');
        return new ViewModel();
    }

}
