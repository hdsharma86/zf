<?php

namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

//use Zend\Form\Element\Csrf;

class CustomerRegistrationForm extends Form {

    public function __construct($name) {

        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'customer_register');
        $this->setAttribute('id', 'customer_register');

        $this->add(array(
            'name' => 'first_name',
            'type' => 'text',
            'options' => array(
                'label' => 'First Name',
                'placeholder' => 'What is your first name?'
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'first_name'
            ),
        ));
        $this->add(array(
            'name' => 'middle_name',
            'type' => 'text',
            'options' => array(
                'label' => 'Middle Name',
                'placeholder' => 'What is your middle name?'
            ),
            'attributes' => array(
                'id' => 'middle_name',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'last_name',
            'type' => 'text',
            'options' => array(
                'label' => 'Last Name',
                'placeholder' => 'What is your last name?'
            ),
            'attributes' => array(
                'id' => 'last_name',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'options' => array(
                'label' => 'Email',
                'placeholder' => 'example@example.com'  
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'email'
            ),
        ));

        $this->add(array(
            'name' => 'address_type',
            'type' => 'Select',
            'options' => array(
                'label' => ' Address Type ',
                'value_options' => array(
                    '' => '-Select Address Type-',
                    "Business" => 'Business',
                    "Self Business" => 'Self Business',
                    "Commerical" => 'Commerical',
                )
            ),
            'attributes' => array(
                'id' => 'address_type',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'country',
            'type' => 'Select',
            'options' => array(
                'label' => 'Country',
                'value_options' => array(
                    '' => '--Select Country--'
                ),
				'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'country',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'address',
            'type' => 'text',
            'options' => array(
                'label' => 'Address',
                'placeholder' => 'What is  your Address?'
            ),
            'attributes' => array(
                'id' => 'address',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'city',
            'type' => 'text',
            'options' => array(
                'label' => 'City',
                'placeholder' => 'What is your City Name ?'
            ),
            'attributes' => array(
                'id' => 'city',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'state',
            'type' => 'Select',
            'options' => array(
                'label' => 'State',
                'value_options' => array(
                    '' => '--Select State--'
                ),
				'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'state',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'postal_code',
            'type' => 'text',
            'options' => array(
                'label' => 'Postal Code',
                'id' => 'postal_code',
                'placeholder' => 'What is your Postal Code ?'
            ),
            'attributes' => array(
                'id' => 'postal_code',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'phone_type',
            'type' => 'Select',
            'options' => array(
                'label' => 'Phone Type',
                'value_options' => array(
                    '' => '--Select Phone Type--',
                    'Personal' => 'Personal',
                    'Business' => 'Business',
                )
            ),
            'attributes' => array(
                'id' => 'phone_type',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'int_prefix',
            'type' => 'text',
            'options' => array(
                'label' => 'International Prefix',
                'placeholder' => 'What is your International Prefix ?'
            ),
            'attributes' => array(
                'id' => 'int_prefix',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'contact_number',
            'type' => 'text',
            'options' => array(
                'label' => 'Phone',
                'placeholder' => 'What is your Phone Number ?'
            ),
            'attributes' => array(
                'id' => 'contact_number',
                'class' => 'form-control'
            ),
        ));
        
        /* $this->add(array(
          'name' => 'password',
          'type' => 'password',
          'options' => array(
          'label' => 'Password',
          'id' => 'password',
          'placeholder' => '**********'
          ),
          'attributes' => array(
          'class' => 'form-control'
          ),
          ));

          $this->add(array(
          'type' => 'Zend\Form\Element\Csrf',
          'name' => 'profileCsrf',
          'options' => array(
          'csrf_options' => array(
          'timeout' => 3600
          )
          )
          )); */

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'class' => 'submitbtn',
                'id' => 'submit',
            ),
        ));
    }

}
