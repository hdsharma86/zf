<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Form;

use Zend\Form\Form;

class BaselineForm extends Form {

    public function __construct($name) {

        parent::__construct($name);
        
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'country_id',
            'type' => 'Select',
            'options' => array(
                'label' => 'Country',
				'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'country',
                'class' => 'form-control'
            ),
        ));
        
        $this->add(array(
            'name' => 'agency_id',
            'type' => 'Select',
            'options' => array(
                'label' => 'Agency',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'agency_id',
                'class' => 'form-control'
            ),
        ));
        
        $this->add(array(
            'name' => 'service_id',
            'type' => 'Select',
            'options' => array(
                'label' => 'Service',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'service_id'
            ),
        ));
        
        $this->add(array(
            'name' => 'proc_service',
            'type' => 'Select',
            'options' => array(
                'label' => 'Common Procurement',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'proc_service',
                'class' => 'form-control has-other-option'
            ),
        ));
        
        $this->add(array(
            'name' => 'baseline_cost',
            'type' => 'text',
            'options' => array(
                'label' => 'Baseline Year- Spending/Cost (US$)',
                'id' => 'by_spending_cost'
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'baseline_cost',
                'placeholder' => 'Your answer'
            ),
        ));
        
        $this->add(array(
            'name' => 'baseline_quantity_purchased',
            'type' => 'text',
            'options' => array(
                'label' => 'Baseline Year- Quantity purchased',
                'id' => 'by_quantity_purchased'
            ),
            'attributes' => array(
                'id' => 'baseline_quantity_purchased',
                'class' => 'form-control',
                'min' => 1,
                'placeholder' => 'Your answer'
            ),
        ));
        
        $this->add(array(
            'name' => 'baseline_service_quality',
            'type' => 'Select',
            'options' => array(
                'label' => 'Baseline Year-Service Quality (Performance Ranking)',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'baseline_service_quality'
            ),
        ));
        
        $this->add(array(
            'name' => 'target_cost_save',
            'type' => 'Select',
            'options' => array(
                'label' => 'Target Year- Cost Savings/Avoidance ($)',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control has-other-option',
                'id' => 'target_cost_save'
            ),
        ));
        
        $this->add(array(
            'name' => 'target_efficiency_gain',
            'type' => 'Select',
            'options' => array(
                'label' => 'Target Year- Efficiency Gains (hrs)',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control has-other-option',
                'id' => 'target_efficiency_gain'
            ),
        ));
        
        $this->add(array(
            'name' => 'target_service_quality',
            'type' => 'Select',
            'options' => array(
                'label' => 'Target Year-Service Quality (Performance Ranking)',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'target_service_quality'
            ),
        ));
         
        $this->add(array(
            'name' => 'managing_entity',
            'type' => 'Select',
            'options' => array(
                'label' => 'Managing Agency',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'managing_entity'
            ),
        ));
        
        $this->add(array(
            'name' => 'clients',
            'type' => 'Select',
            'options' => array(
                'label' => 'Clients',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'clients',
                'class' => 'form-control',
                'multiple' => 'multiple'
            ),
        ));
        
        $this->add(array(
            'name' => 'modality',
            'type' => 'Select',
            'options' => array(
                'label' => 'Outsources',
                'value_options' => array(
                    '' => '--Select Outsources--',
                    'Long Term Agreement (LTA)' => 'Long Term Agreement (LTA)',
                    'Contracts' => 'Contracts'
                ),
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'modality',
                'class' => 'form-control',
            ),
        ));
        
        $this->add(array(
            'name' => 'modality_type',
            'type' => 'Select',
            'options' => array(
                'label' => 'In-house',
                'value_options' => array(
                    '' => '--Select In-house--',
                    'MoU' => 'MoU',
                    'Shared Contract' => 'Shared Contract'
                ),
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'modality_type',
                'class' => 'form-control',
            ),
        ));
        
        $this->add(array(
            'name' => 'assessment_action',
            'type' => 'Select',
            'options' => array(
                'label' => 'Recommended Action',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'assessment_action',
                'class' => 'form-control',
            ),
        ));
        
        $this->add(array(
            'name' => 'additional_comment',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Additional Comments',
                'id' => 'additional_comment'
            ),
            'attributes' => array(
                'id' => 'additional_comment',
                'class' => 'form-control',
                'placeholder' => 'Your answer'
            ),
        ));
        
        $this->add(array(
            'name' => 'needs_justification',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Need Justification',
                'id' => 'needs_justification'
            ),
            'attributes' => array(
                'id' => 'needs_justification',
                'class' => 'form-control',
                'placeholder' => 'Your answer'
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
