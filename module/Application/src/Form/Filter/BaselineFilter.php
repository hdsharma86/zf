<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Form\Filter;

use Zend\InputFilter\InputFilter;

class BaselineFilter extends InputFilter
{

    public function __construct()
    {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
        
        $this->add([
            'name' => 'country_id',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select country.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'agency_id',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select agency.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'service_id',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select common service.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'proc_service',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Select existing procurement service.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        
        $this->add([
            'name' => 'baseline_cost',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Baseline Year- Spending/Cost can not be empty.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        
        $this->add([
            'name' => 'baseline_quantity_purchased',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Baseline Year- Quantity purchased can not be empty.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'baseline_service_quality',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Baseline Year-Service Quality (Performance Ranking).'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'target_cost_save',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Target Year- Cost Savings/Avoidance.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'target_service_quality',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Target Year- Cost Savings/Avoidance Ranking.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'target_efficiency_gain',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Target Year- Cost Savings/Avoidance'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'managing_entity',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Managing Entity (Service Manager).'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'clients',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select Clients Agencies using the Services (separated by commas).'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'modality',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select outsources.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'modality_type',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select In-house.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'assessment_action',
            'required' => true,
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Please select recommended action.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'additional_comment',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Additional Comments can not be empty.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
        $this->add([
            'name' => 'needs_justification',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty',
                    'options' => [
                        'messages' => [
                            $isEmpty => 'Need Justification can not be empty.'
                        ]
                    ],
                    'break_chain_on_failure' => true
                ]
            ]
        ]);
    }
}