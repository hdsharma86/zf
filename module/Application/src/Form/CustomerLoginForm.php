<?php

namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

//use Zend\Form\Element\Csrf;

class CustomerLoginForm extends Form {

    public function __construct($name) {

        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'customer_login');
        $this->setAttribute('id', 'customer_login');

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'options' => array(
                'label' => 'Email',
            ),
            'attributes' => array(
                'placeholder' => 'Email',
                'id' => 'email'
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'options' => array(
                'label' => 'Password',
                'id' => 'password',
            ),
            'attributes' => array(
                'placeholder' => 'Password',
            ),
        ));

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Csrf',
//            'name' => 'profileCsrf',
//            'options' => array(
//                'csrf_options' => array(
//                    'timeout' => 3600
//                )
//            )
//        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Login',
                'id' => 'submit',
            ),
        ));
    }

}
