<?php

namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

//use Zend\Form\Element\Csrf;

class CustomerForgotPassForm extends Form {

    public function __construct($name) {

        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'forgot_password');
        $this->setAttribute('id', 'forgot_password');

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'options' => array(
                'label' => 'Email',
            ),
            'attributes' => array(
                'placeholder' => 'Email',
                'id' => 'email'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Reset',
                'id' => 'submit',
            ),
        ));
    }

}
