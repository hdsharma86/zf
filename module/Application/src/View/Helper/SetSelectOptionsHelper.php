<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

// This view helper class displays a menu bar.
class SetSelectOptionsHelper extends AbstractHelper
{

    // Option items array.
    protected $items = [];
    protected $defaultOption = [];
    protected $value = '';
    public static $instance;
    // Constructor.
    public function __construct($items = []) {
        $this->items = $items;
        self::$instance = $this;
    }

    public function setItems($items, $default = '', $value=''){
        $this->items = $items;
        $this->defaultOption = $default;
        $this->value = $value;
        return self::$instance;
    }
    
    // Set options.
    public function render(){
        $options = '';
        if(!empty($this->items)){
            if(!empty($this->defaultOption)){
                $options .= '<option value="">'.$this->defaultOption.'</option>';
            }
            foreach ($this->items as $key=>$value){
                if($this->value == $key || $this->value == $value){
                    $options .= '<option selected="selected" value="'.$key.'">'.$value.'</option>';
                } else {
                    $options .= '<option value="'.$key.'">'.$value.'</option>';
                }
			}
		}
		echo $options;
    }
    
}