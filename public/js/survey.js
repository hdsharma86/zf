(function( $ ){
	
	/*ajaxRequest({'action':'get_countries'}, 'baseline_country');
	ajaxRequest({'action':'get_agencies'}, 'baseline_agency');
	ajaxRequest({'action':'get_services'}, 'baseline_common_service');*/
	
	$(document).on('change', 'select#baseline_common_service', function(){
		$("#survey_id, #baseline_qty_purchased,#baseline_year,#baseline_year_cost,#baseline_year_service_quality,#target_year,#target_year_efficiency_gain,#target_year_cost,#target_year_service_quality,#managing_entity,#modality,#madality_type,#clients,#recommended_action,#additional_comment").val('');
		var parentID = $(this).val();
		ajaxRequest({'action':'get_existing_services', 'parent_id':parentID}, 'baseline_existing_service', 'set_select_options');
	});

    $(document).on('change', 'select#processcost_common_service', function(){
        $("#process_step, #staff_involved,#time_required,#is_lta,#grade_level").val('');
        var parentID = $(this).val();
        ajaxRequest({'action':'get_existing_services', 'parent_id':parentID}, 'processcost_existing_service', 'set_select_options');
    });

	$(document).on('change', 'select#modality', function(){
		setModalityType();	
	});
	
	$(document).on('change', 'select#baseline_existing_service', function(){
		
		var customerID = $("#customer_id").val();
		var serviceArea = $('#baseline_common_service').val();
		var subServiceArea = $(this).val();
		ajaxRequest({'action':'get_existing_customer_survey', 'service_area':serviceArea, 'child_service_area':subServiceArea, 'customer_id':customerID}, '', 'get_existing_customer_survey');
		
	});
	
})( jQuery );

function ajaxRequest(params, elm, action){
	var response;
	$.ajax({
        type: 'POST',
        url: "/ajax-requests",
        data: params,
        dataType: 'json',
        success: function (res) {
        	if(action == 'set_select_options'){
        		setSelectOptions(elm, res);
        	} else if(action == 'get_existing_customer_survey') {
        		if(res.status == 'SUCCESS'){
        			$("#survey_id").val(res.data.id);
        			$("#baseline_country").val(res.data.country_id);
        			$("#baseline_agency").val(res.data.agency_id);
        			$("#baseline_year").val(res.data.baseline_year);
        			$("#baseline_qty_purchased").val(res.data.baseline_quantity_purchased);
        			$("#baseline_year_cost").val(res.data.baseline_cost);
        			$("#baseline_year_service_quality").val(res.data.baseline_service_quality);
        			$("#target_year").val(res.data.target_year);
        			$("#target_year_efficiency_gain").val(res.data.target_efficiency_gain);
        			$("#target_year_cost").val(res.data.target_cost_save);
        			$("#target_year_service_quality").val(res.data.target_service_quality);
        			$("#managing_entity").val(res.data.managing_entity);
        			$("#modality").val(res.data.modality);
        			setModalityType();
        			$("#madality_type").val(res.data.modality_type);
        			
        			var values = res.data.clients;
        			$.each(values.split(","), function(i,e){
        			    $("#clients option[value='" + e + "']").prop("selected", true);
        			});
        			
        			$("#recommended_action").val(res.data.assessment_action);
        			$("#additional_comment").val(res.data.additional_comment);    	
        		} else {
        			$("#survey_id, #baseline_country,#baseline_agency,#baseline_qty_purchased,#baseline_year,#baseline_year_cost,#baseline_year_service_quality,#target_year,#target_year_efficiency_gain,#target_year_cost,#target_year_service_quality,#managing_entity,#modality,#madality_type,#clients,#recommended_action,#additional_comment").val(''); 
        		}        				
        	}
        }
    });
	return response;
}

function setSelectOptions(elm, options){
	$('#'+elm).empty();
	$.each(options.data, function(key,value) {
		$('#'+elm).append($("<option></option>").attr("value", key).text(value));
	});
}

function setModalityType(){
	var modality = $('select#modality').val();
	var element = $('#madality_type');
	element.empty();
	element.append($("<option></option>").attr("value", '').text('-Select Modality Type-'));
	if( modality == 'Outsources' ){
		element.append($("<option></option>").attr("value", 'Long Term Agreement(LTA)').text('Long Term Agreement(LTA)'));
		element.append($("<option></option>").attr("value", 'Contracts').text('Contracts'));
	} else if(modality == 'In-house'){
		element.append($("<option></option>").attr("value", 'MoU').text('MoU'));
		element.append($("<option></option>").attr("value", 'Shared Contract').text('Shared Contract'));
	}
	return true;
}