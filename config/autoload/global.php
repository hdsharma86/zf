<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
		
   	'module_layouts' => array(
    	'Application' => 'layout/layout.phtml',
      	'Admin' => 'layout/admin.phtml',
      	'Calculator' => 'layout/calculator.phtml',
      	'Consultant' => 'layout/consultant.phtml',
      	'Common' => 'layout/common.phtml'
   	),	
		
	'db' => array(
		'driver'         => 'Pdo',
		'dsn'            => 'mysql:dbname=boms;host=localhost',
		'username'		=> 'root',
		'password'		=> 'hardev123',
		'driver_options' => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
		),
	),
		
	'service_manager' => array(
        'factories' => array(
            //'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'Zend\Db\Adapter\Adapter' => function ($serviceManager) {
	            $adapterFactory = new Zend\Db\Adapter\AdapterServiceFactory();
	               $adapter = $adapterFactory->createService($serviceManager);
	               // set static adapter
	               \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);

	               return $adapter;
	         }
        ),
        'alias' => array(
            'Zend\Authentication\AuthenticationService' => 'my_auth_service',
        ),
        'invokables' => array(
            'my_auth_service' => 'Zend\Authentication\AuthenticationService',
        ),
    ),

    'constants' => array(
		'TEST'   => 'test_value',
	),
		
];
